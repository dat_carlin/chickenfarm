<Q                         INSTANCING_ON      _MAIN_LIGHT_SHADOWS    _MAIN_LIGHT_SHADOWS_CASCADE     {  #ifdef VERTEX
#version 300 es
#ifndef UNITY_RUNTIME_INSTANCING_ARRAY_SIZE
	#define UNITY_RUNTIME_INSTANCING_ARRAY_SIZE 2
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ScreenParams;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	int unity_BaseInstanceID;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM mediump vec4 unity_WorldTransformParams;
	UNITY_UNIFORM mediump vec4 unity_LightData;
	UNITY_UNIFORM mediump vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM mediump vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_LightmapIndex;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM mediump vec4 unity_SHAr;
	UNITY_UNIFORM mediump vec4 unity_SHAg;
	UNITY_UNIFORM mediump vec4 unity_SHAb;
	UNITY_UNIFORM mediump vec4 unity_SHBr;
	UNITY_UNIFORM mediump vec4 unity_SHBg;
	UNITY_UNIFORM mediump vec4 unity_SHBb;
	UNITY_UNIFORM mediump vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
struct unity_Builtins0Array_Type {
	vec4 hlslcc_mtx4x4unity_ObjectToWorldArray[4];
	vec4 hlslcc_mtx4x4unity_WorldToObjectArray[4];
};
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityInstancing_PerDraw0 {
#endif
	UNITY_UNIFORM unity_Builtins0Array_Type unity_Builtins0Array[UNITY_RUNTIME_INSTANCING_ARRAY_SIZE];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(2) uniform UnityPerMaterial {
#endif
	UNITY_UNIFORM mediump float _RampSmoothing;
	UNITY_UNIFORM mediump float _RampThreshold;
	UNITY_UNIFORM mediump float _RampBands;
	UNITY_UNIFORM mediump float _RampBandsSmoothing;
	UNITY_UNIFORM mediump float _RampScale;
	UNITY_UNIFORM mediump float _RampOffset;
	UNITY_UNIFORM vec4 _BumpMap_ST;
	UNITY_UNIFORM mediump float _BumpScale;
	UNITY_UNIFORM vec4 _BaseMap_ST;
	UNITY_UNIFORM mediump float _Cutoff;
	UNITY_UNIFORM mediump vec4 _BaseColor;
	UNITY_UNIFORM vec4 _EmissionMap_ST;
	UNITY_UNIFORM mediump float _EmissionChannel;
	UNITY_UNIFORM mediump vec4 _EmissionColor;
	UNITY_UNIFORM mediump vec4 _MatCapColor;
	UNITY_UNIFORM mediump float _MatCapMaskChannel;
	UNITY_UNIFORM mediump float _MatCapType;
	UNITY_UNIFORM mediump vec4 _SColor;
	UNITY_UNIFORM mediump vec4 _HColor;
	UNITY_UNIFORM mediump float _RimMin;
	UNITY_UNIFORM mediump float _RimMax;
	UNITY_UNIFORM mediump vec4 _RimColor;
	UNITY_UNIFORM mediump float _SpecularRoughness;
	UNITY_UNIFORM mediump vec4 _SpecularColor;
	UNITY_UNIFORM mediump float _SpecularMapType;
	UNITY_UNIFORM mediump float _SpecularToonSize;
	UNITY_UNIFORM mediump float _SpecularToonSmoothness;
	UNITY_UNIFORM mediump float _ReflectionSmoothness;
	UNITY_UNIFORM mediump vec4 _ReflectionColor;
	UNITY_UNIFORM mediump float _FresnelMax;
	UNITY_UNIFORM mediump float _FresnelMin;
	UNITY_UNIFORM mediump float _ReflectionMapType;
	UNITY_UNIFORM mediump float _OcclusionStrength;
	UNITY_UNIFORM mediump float _OcclusionChannel;
	UNITY_UNIFORM mediump float _IndirectIntensity;
	UNITY_UNIFORM mediump float _SingleIndirectColor;
	UNITY_UNIFORM mediump float _OutlineWidth;
	UNITY_UNIFORM mediump float _OutlineMinWidth;
	UNITY_UNIFORM mediump float _OutlineMaxWidth;
	UNITY_UNIFORM mediump vec4 _OutlineColor;
	UNITY_UNIFORM mediump float _OutlineTextureLOD;
	UNITY_UNIFORM mediump float _DirectIntensityOutline;
	UNITY_UNIFORM mediump float _IndirectIntensityOutline;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD1;
flat out highp uint vs_SV_InstanceID0;
vec4 u_xlat0;
vec4 u_xlat1;
mediump float u_xlat16_1;
vec2 u_xlat2;
vec4 u_xlat3;
vec3 u_xlat4;
float u_xlat8;
int u_xlati8;
void main()
{
    u_xlat0.xyz = in_NORMAL0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_NORMAL0.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_NORMAL0.zzz + u_xlat0.xyz;
    u_xlat4.xz = u_xlat0.yy * hlslcc_mtx4x4unity_MatrixVP[1].xy;
    u_xlat0.xy = hlslcc_mtx4x4unity_MatrixVP[0].xy * u_xlat0.xx + u_xlat4.xz;
    u_xlat0.xy = hlslcc_mtx4x4unity_MatrixVP[2].xy * u_xlat0.zz + u_xlat0.xy;
    u_xlat8 = dot(u_xlat0.xy, u_xlat0.xy);
    u_xlat8 = inversesqrt(u_xlat8);
    u_xlat0.xy = vec2(u_xlat8) * u_xlat0.xy;
    u_xlat16_1 = _OutlineWidth * 0.00999999978;
    u_xlat0.xy = u_xlat0.xy * vec2(u_xlat16_1);
    u_xlat2.y = _ScreenParams.x / _ScreenParams.y;
    u_xlat2.x = 1.0;
    u_xlati8 = gl_InstanceID + unity_BaseInstanceID;
    u_xlati8 = int(u_xlati8 << 3);
    u_xlat1 = in_POSITION0.yyyy * unity_Builtins0Array[u_xlati8 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[1];
    u_xlat1 = unity_Builtins0Array[u_xlati8 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[0] * in_POSITION0.xxxx + u_xlat1;
    u_xlat1 = unity_Builtins0Array[u_xlati8 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[2] * in_POSITION0.zzzz + u_xlat1;
    u_xlat1 = u_xlat1 + unity_Builtins0Array[u_xlati8 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[3];
    u_xlat3 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat3 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat3;
    u_xlat3 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat3;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat3;
    gl_Position.xy = u_xlat0.xy * u_xlat2.xy + u_xlat1.xy;
    gl_Position.zw = u_xlat1.zw;
    vs_TEXCOORD0 = _OutlineColor;
    u_xlat0.xyw = in_POSITION0.yyy * unity_Builtins0Array[u_xlati8 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[1].xyz;
    u_xlat0.xyw = unity_Builtins0Array[u_xlati8 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[0].xyz * in_POSITION0.xxx + u_xlat0.xyw;
    u_xlat0.xyw = unity_Builtins0Array[u_xlati8 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[2].xyz * in_POSITION0.zzz + u_xlat0.xyw;
    vs_TEXCOORD1.xyz = u_xlat0.xyw + unity_Builtins0Array[u_xlati8 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[3].xyz;
    vs_TEXCOORD1.w = 0.0;
    vs_SV_InstanceID0 = uint(gl_InstanceID);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
in highp vec4 vs_TEXCOORD0;
layout(location = 0) out highp vec4 SV_Target0;
void main()
{
    SV_Target0 = vs_TEXCOORD0;
    return;
}

#endif
                              $GlobalsT         _ScreenParams                            unity_BaseInstanceID                 P      unity_MatrixVP                             UnityPerDraw�        unity_LODFade                     �      unity_WorldTransformParams                    �      unity_LightData                   �      unity_LightIndices                   �      unity_ProbesOcclusion                     �      unity_SpecCube0_HDR                   �      unity_LightmapST                  �      unity_LightmapIndex                         unity_DynamicLightmapST                     
   unity_SHAr                       
   unity_SHAg                    0  
   unity_SHAb                    @  
   unity_SHBr                    P  
   unity_SHBg                    `  
   unity_SHBb                    p  	   unity_SHC                     �     unity_ObjectToWorld                         unity_WorldToObject                  @          UnityInstancing_PerDraw0             unity_Builtins0Array       �         unity_ObjectToWorldArray                        unity_WorldToObjectArray                 @      UnityPerMaterial�  +      _RampSmoothing                           _RampThreshold                       
   _RampBands                          _RampBandsSmoothing                      
   _RampScale                          _RampOffset                         _BumpMap_ST                       
   _BumpScale                    0      _BaseMap_ST                   @      _Cutoff                   P   
   _BaseColor                    `      _EmissionMap_ST                   p      _EmissionChannel                  �      _EmissionColor                    �      _MatCapColor                  �      _MatCapMaskChannel                    �      _MatCapType                   �      _SColor                   �      _HColor                   �      _RimMin                   �      _RimMax                   �   	   _RimColor                     �      _SpecularRoughness                          _SpecularColor                         _SpecularMapType                        _SpecularToonSize                     $     _SpecularToonSmoothness                   (     _ReflectionSmoothness                     ,     _ReflectionColor                  0     _FresnelMax                   @     _FresnelMin                   D     _ReflectionMapType                    H     _OcclusionStrength                    L     _OcclusionChannel                     P     _IndirectIntensity                    T     _SingleIndirectColor                  X     _OutlineWidth                     \     _OutlineMinWidth                  `     _OutlineMaxWidth                  d     _OutlineColor                     p     _OutlineTextureLOD                    �     _DirectIntensityOutline                   �     _IndirectIntensityOutline                     �            UnityPerDraw              UnityInstancing_PerDraw0             UnityPerMaterial          