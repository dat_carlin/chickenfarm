<Q                         USE_S       �!  #ifdef VERTEX
#version 150
#extension GL_ARB_explicit_attrib_location : require
#ifdef GL_ARB_shader_bit_encoding
#extension GL_ARB_shader_bit_encoding : enable
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
UNITY_BINDING(0) uniform UnityPerDraw {
	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	vec4 unity_LODFade;
	vec4 unity_WorldTransformParams;
	vec4 unity_LightData;
	vec4 unity_LightIndices[2];
	vec4 unity_ProbesOcclusion;
	vec4 unity_SpecCube0_HDR;
	vec4 unity_LightmapST;
	vec4 unity_DynamicLightmapST;
	vec4 unity_SHAr;
	vec4 unity_SHAg;
	vec4 unity_SHAb;
	vec4 unity_SHBr;
	vec4 unity_SHBg;
	vec4 unity_SHBb;
	vec4 unity_SHC;
};
in  vec4 in_POSITION0;
in  vec2 in_TEXCOORD0;
in  float in_COLOR0;
in  vec3 in_NORMAL0;
out vec3 vs_TEXCOORD3;
out float vs_TEXCOORD2;
out vec2 vs_TEXCOORD0;
out float vs_TEXCOORD1;
out vec3 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    vs_TEXCOORD2 = in_COLOR0;
    u_xlat0.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    vs_TEXCOORD3.xyz = u_xlat0.xyz;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    vs_TEXCOORD1 = 0.0;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = u_xlat0 + hlslcc_mtx4x4unity_MatrixVP[3];
    vs_TEXCOORD4.xyz = in_NORMAL0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 150
#extension GL_ARB_explicit_attrib_location : require
#ifdef GL_ARB_shader_bit_encoding
#extension GL_ARB_shader_bit_encoding : enable
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _MainLightColor;
uniform 	vec4 _Time;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	float _TilingN1;
uniform 	float _TilingN2;
uniform 	float _WindForce;
uniform 	vec4 _Color;
uniform 	vec4 _SelfShadowColor;
uniform 	vec4 _GroundColor;
uniform 	float _TilingN3;
uniform 	float _WindMovement;
uniform 	float _GrassThinness;
uniform 	float _GrassShading;
uniform 	float _GrassThinnessIntersection;
uniform 	float _NoisePower;
uniform 	float _GrassSaturation;
uniform 	float _FadeDistanceStart;
uniform 	float _FadeDistanceEnd;
uniform 	float _GrassCut;
UNITY_LOCATION(0) uniform  sampler2D _GrassTex;
UNITY_LOCATION(1) uniform  sampler2D _MainTex;
UNITY_LOCATION(2) uniform  sampler2D _NoGrassTex;
UNITY_LOCATION(3) uniform  sampler2D _Distortion;
UNITY_LOCATION(4) uniform  sampler2D _Noise;
UNITY_LOCATION(5) uniform  sampler2D _GroundTex;
in  vec3 vs_TEXCOORD3;
in  float vs_TEXCOORD2;
in  vec2 vs_TEXCOORD0;
layout(location = 0) out vec4 SV_Target0;
vec4 u_xlat0;
ivec2 u_xlati0;
bool u_xlatb0;
vec4 u_xlat1;
vec4 u_xlat2;
vec4 u_xlat3;
vec4 u_xlat4;
vec3 u_xlat5;
bool u_xlatb5;
vec3 u_xlat8;
vec2 u_xlat10;
bool u_xlatb10;
float u_xlat15;
bool u_xlatb15;
float u_xlat17;
void main()
{
    u_xlat0.x = (-_FadeDistanceStart) + _FadeDistanceEnd;
    u_xlat0.x = max(u_xlat0.x, 9.99999975e-05);
    u_xlat0.x = float(1.0) / u_xlat0.x;
    u_xlat5.xyz = (-vs_TEXCOORD3.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat5.x = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat0.y = sqrt(u_xlat5.x);
    u_xlat10.x = u_xlat0.y + (-_FadeDistanceStart);
    u_xlat0.x = (-u_xlat10.x) * u_xlat0.x + 1.0;
    u_xlati0.xy = ivec2(uvec2(lessThan(vec4(0.100000001, 0.0, 0.0, 0.0), u_xlat0.xyxx).xy) * 0xFFFFFFFFu);
    u_xlati0.x = (u_xlati0.y != 0) ? u_xlati0.x : int(0xFFFFFFFFu);
    u_xlatb5 = vs_TEXCOORD2>=0.00999999978;
    u_xlati0.x = u_xlatb5 ? u_xlati0.x : int(0);
    u_xlat5.x = _Time.y * 0.5 + 1.051;
    u_xlat10.x = _Time.x * _WindMovement;
    u_xlat10.x = u_xlat10.x * 3.0;
    u_xlat10.xy = vs_TEXCOORD0.xy * vec2(_TilingN3) + u_xlat10.xx;
    u_xlat1 = texture(_Distortion, u_xlat10.xy);
    u_xlat5.y = u_xlat1.x * 5.0 + _Time.y;
    u_xlat5.xy = sin(u_xlat5.xy);
    u_xlat5.x = u_xlat5.x + u_xlat5.y;
    u_xlat10.x = u_xlat1.x * 0.150000006;
    u_xlat5.x = u_xlat5.x * 0.200000003 + u_xlat10.x;
    u_xlat5.x = u_xlat5.x * 0.600000024;
    u_xlat1 = u_xlat5.xxxx * u_xlat1.xyxy;
    u_xlat5.x = vs_TEXCOORD2 * 1.29999995;
    u_xlat1 = u_xlat5.xxxx * u_xlat1;
    u_xlat1 = u_xlat1 * vec4(vec4(_WindForce, _WindForce, _WindForce, _WindForce));
    u_xlat2 = vs_TEXCOORD0.xyxy * vec4(_TilingN1, _TilingN1, _TilingN2, _TilingN2) + u_xlat1;
    u_xlat3 = texture(_GrassTex, u_xlat2.xy);
    u_xlat2 = texture(_Noise, u_xlat2.zw);
    u_xlat5.x = u_xlat2.x * _NoisePower;
    u_xlat5.x = dot(u_xlat5.xx, u_xlat1.zz);
    u_xlat2 = u_xlat1 * vec4(0.0900000036, 0.0900000036, 0.0500000007, 0.0500000007) + vs_TEXCOORD0.xyxy;
    u_xlat10.x = u_xlat1.z * 5.0;
    u_xlat1 = texture(_MainTex, u_xlat2.xy);
    u_xlat15 = u_xlat3.x + u_xlat1.x;
    u_xlat15 = u_xlat15 * _GrassThinness;
    u_xlat4 = texture(_NoGrassTex, u_xlat2.zw);
    u_xlat2 = texture(_GroundTex, u_xlat2.zw);
    u_xlat4.x = u_xlat4.x;
    u_xlat4.x = clamp(u_xlat4.x, 0.0, 1.0);
    u_xlat8.xyz = (-vec3(vs_TEXCOORD2)) + vec3(2.0, 1.0, 1.25);
    u_xlat17 = u_xlat4.x * u_xlat8.x;
    u_xlat17 = u_xlat3.x * u_xlat17;
    u_xlat3.x = u_xlat3.x * u_xlat4.x;
    u_xlat8.x = (-u_xlat4.x) + 1.0;
    u_xlat3.x = u_xlat3.x * u_xlat8.y;
    u_xlat15 = (-u_xlat15) * u_xlat17 + 1.0;
    u_xlat10.x = u_xlat3.x * _GrassThinness + (-u_xlat10.x);
    u_xlat17 = u_xlat3.x * _GrassThinnessIntersection;
    u_xlatb10 = u_xlat10.x>=u_xlat15;
    u_xlat10.x = u_xlatb10 ? 1.0 : float(0.0);
    u_xlat10.x = u_xlat8.x * u_xlat17 + u_xlat10.x;
    u_xlat15 = u_xlat10.x * u_xlat8.z + -0.0199999996;
    u_xlatb15 = u_xlat15<0.0;
    u_xlati0.x = u_xlatb15 ? u_xlati0.x : int(0);
    if((u_xlati0.x)!=0){discard;}
    u_xlat0.x = max(_GrassCut, 0.00100000005);
    u_xlat0.x = u_xlat10.x / u_xlat0.x;
    u_xlat0.x = u_xlat0.x + -0.0199999996;
    u_xlatb0 = u_xlat0.x<0.0;
    u_xlatb10 = 0.0<_GrassCut;
    u_xlatb0 = u_xlatb10 && u_xlatb0;
    if(((int(u_xlatb0) * int(0xffffffffu)))!=0){discard;}
    u_xlat0.x = _GrassShading * 1.25 + (-u_xlat5.x);
    u_xlat10.x = log2(vs_TEXCOORD2);
    u_xlat10.x = u_xlat10.x * 1.10000002;
    u_xlat10.x = exp2(u_xlat10.x);
    u_xlat3.xzw = (-_SelfShadowColor.xyz) + vec3(1.0, 1.0, 1.0);
    u_xlat3.xzw = u_xlat10.xxx * u_xlat3.xzw + _SelfShadowColor.xyz;
    u_xlat0.xzw = u_xlat0.xxx + u_xlat3.xzw;
    u_xlat0.xzw = u_xlat8.xxx + u_xlat0.xzw;
    u_xlat0.xyz = (-u_xlat5.xxx) + u_xlat0.xzw;
    u_xlat0.xyz = clamp(u_xlat0.xyz, 0.0, 1.0);
    u_xlat1.xyz = log2(u_xlat1.xyz);
    SV_Target0.w = u_xlat1.w;
    u_xlat1.xyz = u_xlat1.xyz * vec3(vec3(_GrassSaturation, _GrassSaturation, _GrassSaturation));
    u_xlat1.xyz = exp2(u_xlat1.xyz);
    u_xlat1.xyz = u_xlat1.xyz * vec3(vec3(_GrassSaturation, _GrassSaturation, _GrassSaturation));
    u_xlat3.xzw = _Color.xyz + _Color.xyz;
    u_xlat1.xyz = u_xlat1.xyz * u_xlat3.xzw;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xyz;
    u_xlat1.xyz = _Color.xyz * vec3(1.95000005, 1.95000005, 1.95000005);
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xyz;
    u_xlat1.xyz = u_xlat8.xxx * _GroundColor.xyz;
    u_xlat1.xyz = u_xlat1.xyz * _GroundColor.xyz;
    u_xlat1.xyz = u_xlat1.xyz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat2.xyz * u_xlat1.xyz + (-u_xlat0.xyz);
    u_xlat1.xyz = u_xlat8.xxx * u_xlat1.xyz + u_xlat0.xyz;
    u_xlatb15 = 0.00999999978>=vs_TEXCOORD2;
    u_xlat0.xyz = (bool(u_xlatb15)) ? u_xlat1.xyz : u_xlat0.xyz;
    u_xlat1.xyz = u_xlat0.xyz * _MainLightColor.xyz;
    u_xlat15 = _MainLightColor.y + _MainLightColor.x;
    u_xlat15 = u_xlat15 + _MainLightColor.z;
    u_xlatb15 = 0.0<u_xlat15;
    SV_Target0.xyz = (bool(u_xlatb15)) ? u_xlat1.xyz : u_xlat0.xyz;
    return;
}

#endif
                              $Globals�         _MainLightColor                          _Time                           _WorldSpaceCameraPos                      	   _TilingN1                     ,   	   _TilingN2                     0   
   _WindForce                    4      _Color                    @      _SelfShadowColor                  P      _GroundColor                  `   	   _TilingN3                     p      _WindMovement                     t      _GrassThinness                    x      _GrassShading                     |      _GrassThinnessIntersection                    �      _NoisePower                   �      _GrassSaturation                  �      _FadeDistanceStart                    �      _FadeDistanceEnd                  �   	   _GrassCut                     �          $GlobalsP         _MainTex_ST                   @      unity_MatrixVP                              UnityPerDraw�        unity_LODFade                     �      unity_WorldTransformParams                    �      unity_LightData                   �      unity_LightIndices                   �      unity_ProbesOcclusion                     �      unity_SpecCube0_HDR                   �      unity_LightmapST                  �      unity_DynamicLightmapST                      
   unity_SHAr                      
   unity_SHAg                       
   unity_SHAb                    0  
   unity_SHBr                    @  
   unity_SHBg                    P  
   unity_SHBb                    `  	   unity_SHC                     p     unity_ObjectToWorld                         unity_WorldToObject                  @          	   _GrassTex                     _MainTex                _NoGrassTex                 _Distortion                 _Noise               
   _GroundTex                  UnityPerDraw           