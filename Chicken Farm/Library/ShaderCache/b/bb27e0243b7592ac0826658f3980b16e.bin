<Q                         USE_RT      o  #ifdef VERTEX
#version 410
#extension GL_ARB_explicit_attrib_location : require

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
UNITY_BINDING(1) uniform UnityPerDraw {
	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	vec4 unity_LODFade;
	vec4 unity_WorldTransformParams;
	vec4 unity_LightData;
	vec4 unity_LightIndices[2];
	vec4 unity_ProbesOcclusion;
	vec4 unity_SpecCube0_HDR;
	vec4 unity_LightmapST;
	vec4 unity_DynamicLightmapST;
	vec4 unity_SHAr;
	vec4 unity_SHAg;
	vec4 unity_SHAb;
	vec4 unity_SHBr;
	vec4 unity_SHBg;
	vec4 unity_SHBb;
	vec4 unity_SHC;
};
UNITY_BINDING(2) uniform MainLightShadows {
	vec4 hlslcc_mtx4x4_MainLightWorldToShadow[20];
	vec4 _CascadeShadowSplitSpheres0;
	vec4 _CascadeShadowSplitSpheres1;
	vec4 _CascadeShadowSplitSpheres2;
	vec4 _CascadeShadowSplitSpheres3;
	vec4 _CascadeShadowSplitSphereRadii;
	vec4 _MainLightShadowOffset0;
	vec4 _MainLightShadowOffset1;
	vec4 _MainLightShadowOffset2;
	vec4 _MainLightShadowOffset3;
	vec4 _MainLightShadowParams;
	vec4 _MainLightShadowmapSize;
};
in  vec4 in_POSITION0;
in  vec2 in_TEXCOORD0;
in  vec3 in_NORMAL0;
layout(location = 0) out vec2 vs_TEXCOORD0;
layout(location = 1) out float vs_TEXCOORD5;
layout(location = 2) out vec4 vs_TEXCOORD1;
layout(location = 3) out vec3 vs_TEXCOORD2;
layout(location = 4) out vec4 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    vs_TEXCOORD5 = 0.0;
    u_xlat0.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = u_xlat1 + hlslcc_mtx4x4unity_MatrixVP[3];
    vs_TEXCOORD1 = in_POSITION0;
    vs_TEXCOORD2.xyz = in_NORMAL0.xyz;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4_MainLightWorldToShadow[1];
    u_xlat1 = hlslcc_mtx4x4_MainLightWorldToShadow[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4_MainLightWorldToShadow[2] * u_xlat0.zzzz + u_xlat1;
    vs_TEXCOORD4 = u_xlat0 + hlslcc_mtx4x4_MainLightWorldToShadow[3];
    return;
}

#endif
#ifdef FRAGMENT
#version 410
#extension GL_ARB_explicit_attrib_location : require

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
vec4 ImmCB_0[4];
uniform 	vec4 _MainLightColor;
uniform 	vec4 _AdditionalLightsCount;
uniform 	vec4 _Time;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec3 _Position;
uniform 	float _OrthographicCamSize;
uniform 	float _HasRT;
uniform 	float _TilingN1;
uniform 	float _TilingN2;
uniform 	float _WindForce;
uniform 	vec4 _Color;
uniform 	vec4 _SelfShadowColor;
uniform 	vec4 _GroundColor;
uniform 	float _TilingN3;
uniform 	float _WindMovement;
uniform 	float _GrassThinness;
uniform 	float _GrassShading;
uniform 	float _GrassThinnessIntersection;
uniform 	vec4 _RimColor;
uniform 	float _RimPower;
uniform 	float _NoisePower;
uniform 	float _GrassSaturation;
uniform 	float _RimMin;
uniform 	float _RimMax;
uniform 	vec4 _Specular0;
uniform 	vec4 _Specular1;
uniform 	vec4 _Specular2;
uniform 	vec4 _Specular3;
uniform 	vec4 _Specular4;
uniform 	vec4 _Specular5;
uniform 	vec4 _Specular6;
uniform 	vec4 _Specular7;
uniform 	vec4 _Splat0_ST;
uniform 	vec4 _Splat1_ST;
uniform 	vec4 _Splat2_ST;
uniform 	vec4 _Splat3_ST;
uniform 	vec4 _Splat4_STn;
uniform 	vec4 _Splat5_STn;
uniform 	vec4 _Splat6_STn;
uniform 	vec4 _Splat7_STn;
uniform 	float _Metallic0;
uniform 	float _Metallic1;
uniform 	float _Metallic2;
uniform 	float _Metallic3;
uniform 	float _Metallic4;
uniform 	float _Metallic5;
uniform 	float _Metallic6;
uniform 	float _Metallic7;
uniform 	float _LightIntensity;
UNITY_BINDING(0) uniform AdditionalLights {
	vec4 _AdditionalLightsPosition[32];
	vec4 _AdditionalLightsColor[32];
	vec4 _AdditionalLightsAttenuation[32];
	vec4 _AdditionalLightsSpotDir[32];
	vec4 _AdditionalLightsOcclusionProbes[32];
};
UNITY_BINDING(1) uniform UnityPerDraw {
	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	vec4 unity_LODFade;
	vec4 unity_WorldTransformParams;
	vec4 unity_LightData;
	vec4 unity_LightIndices[2];
	vec4 unity_ProbesOcclusion;
	vec4 unity_SpecCube0_HDR;
	vec4 unity_LightmapST;
	vec4 unity_DynamicLightmapST;
	vec4 unity_SHAr;
	vec4 unity_SHAg;
	vec4 unity_SHAb;
	vec4 unity_SHBr;
	vec4 unity_SHBg;
	vec4 unity_SHBb;
	vec4 unity_SHC;
};
UNITY_LOCATION(0) uniform  sampler2D _GlobalEffectRT;
UNITY_LOCATION(1) uniform  sampler2D _Control0;
UNITY_LOCATION(2) uniform  sampler2D _Control1;
UNITY_LOCATION(3) uniform  sampler2D _NoGrassTex;
UNITY_LOCATION(4) uniform  sampler2D _Distortion;
UNITY_LOCATION(5) uniform  sampler2D _Noise;
UNITY_LOCATION(6) uniform  sampler2D _Splat0;
UNITY_LOCATION(7) uniform  sampler2D _Splat1;
UNITY_LOCATION(8) uniform  sampler2D _Splat2;
UNITY_LOCATION(9) uniform  sampler2D _Splat3;
UNITY_LOCATION(10) uniform  sampler2D _Splat4;
UNITY_LOCATION(11) uniform  sampler2D _Splat5;
UNITY_LOCATION(12) uniform  sampler2D _Splat6;
UNITY_LOCATION(13) uniform  sampler2D _Splat7;
UNITY_LOCATION(14) uniform  sampler2D _Normal1;
UNITY_LOCATION(15) uniform  sampler2D _Normal2;
UNITY_LOCATION(16) uniform  sampler2D _Normal3;
UNITY_LOCATION(17) uniform  sampler2D _Normal4;
UNITY_LOCATION(18) uniform  sampler2D _Normal5;
UNITY_LOCATION(19) uniform  sampler2D _Normal6;
UNITY_LOCATION(20) uniform  sampler2D _Normal7;
layout(location = 0) in  vec2 gs_TEXCOORD0;
layout(location = 1) in  float gs_TEXCOORD2;
layout(location = 2) in  vec3 gs_TEXCOORD1;
layout(location = 3) in  vec3 gs_TEXCOORD3;
layout(location = 0) out vec4 SV_Target0;
vec4 u_xlat0;
vec4 u_xlat1;
vec4 u_xlat2;
int u_xlati2;
bool u_xlatb2;
vec4 u_xlat3;
vec4 u_xlat4;
vec4 u_xlat5;
vec4 u_xlat6;
vec4 u_xlat7;
vec4 u_xlat8;
vec4 u_xlat9;
vec4 u_xlat10;
vec4 u_xlat11;
vec4 u_xlat12;
vec4 u_xlat13;
vec3 u_xlat14;
vec4 u_xlat15;
vec4 u_xlat16;
vec4 u_xlat17;
vec4 u_xlat18;
vec4 u_xlat19;
vec4 u_xlat20;
vec4 u_xlat21;
bool u_xlatb22;
vec2 u_xlat23;
vec3 u_xlat24;
uint u_xlatu24;
float u_xlat25;
float u_xlat44;
bool u_xlatb44;
vec2 u_xlat45;
bool u_xlatb45;
vec2 u_xlat48;
vec2 u_xlat59;
float u_xlat66;
uint u_xlatu66;
bool u_xlatb66;
float u_xlat67;
int u_xlati67;
float u_xlat70;
float u_xlat71;
uint u_xlatu71;
bool u_xlatb71;
float u_xlat72;
int u_xlati72;
bool u_xlatb72;
float u_xlat73;
uint u_xlatu73;
float u_xlat74;
float u_xlat75;
void main()
{
ImmCB_0[0] = vec4(1.0,0.0,0.0,0.0);
ImmCB_0[1] = vec4(0.0,1.0,0.0,0.0);
ImmCB_0[2] = vec4(0.0,0.0,1.0,0.0);
ImmCB_0[3] = vec4(0.0,0.0,0.0,1.0);
    u_xlat0.xy = gs_TEXCOORD1.xz + (-_Position.xz);
    u_xlat44 = _OrthographicCamSize + _OrthographicCamSize;
    u_xlat0.xy = u_xlat0.xy / vec2(u_xlat44);
    u_xlat0.xy = u_xlat0.xy + vec2(0.5, 0.5);
    u_xlatb44 = vec4(0.0, 0.0, 0.0, 0.0)!=vec4(_HasRT);
    if(u_xlatb44){
        u_xlat1 = texture(_GlobalEffectRT, u_xlat0.xy);
        u_xlat66 = u_xlat1.z * 5.0;
        u_xlat66 = max(u_xlat66, 0.0);
        u_xlat66 = min(u_xlat66, 2.0);
        u_xlat66 = (-u_xlat66) + 1.0;
    } else {
        u_xlat66 = 1.0;
    }
    u_xlat1.x = _Time.x * 3.0;
    u_xlat1.x = u_xlat1.x * _WindMovement;
    u_xlat1.xy = gs_TEXCOORD0.xy * vec2(_TilingN3) + u_xlat1.xx;
    u_xlat1 = texture(_Distortion, u_xlat1.xy);
    u_xlat45.x = u_xlat1.x * 0.150000006;
    u_xlat67 = u_xlat1.x * 5.0 + _Time.y;
    u_xlat67 = sin(u_xlat67);
    u_xlat2.x = _Time.y * 0.5 + 1.051;
    u_xlat2.x = sin(u_xlat2.x);
    u_xlat67 = u_xlat67 + u_xlat2.x;
    u_xlat45.x = u_xlat67 * 0.200000003 + u_xlat45.x;
    u_xlat45.x = u_xlat66 * u_xlat45.x;
    u_xlat45.x = u_xlat45.x * 0.600000024;
    u_xlat1.xy = u_xlat45.xx * u_xlat1.xy;
    u_xlat45.x = gs_TEXCOORD2 * 1.29999995;
    u_xlat1.xy = u_xlat45.xx * u_xlat1.xy;
    u_xlat1.xy = u_xlat1.xy * vec2(vec2(_WindForce, _WindForce));
    u_xlat1.xy = vec2(u_xlat66) * u_xlat1.xy;
    if(u_xlatb44){
        u_xlat0.xy = u_xlat1.xy * vec2(0.0399999991, 0.0399999991) + u_xlat0.xy;
        u_xlat0 = texture(_GlobalEffectRT, u_xlat0.xy);
        u_xlat66 = u_xlat0.x * (-u_xlat0.x);
        u_xlat0.xyz = u_xlat0.zxy * vec3(-1.0, 1.0, -1.0) + vec3(0.25, 0.0, 0.0);
    } else {
        u_xlat0.x = float(0.25);
        u_xlat0.y = float(0.0);
        u_xlat0.z = float(0.0);
        u_xlat66 = float(0.0);
    }
    u_xlat45.xy = u_xlat1.xy * vec2(0.0500000007, 0.0500000007) + gs_TEXCOORD0.xy;
    u_xlat2 = texture(_Control0, u_xlat45.xy);
    u_xlat3 = texture(_Control1, u_xlat45.xy);
    u_xlat4.xy = gs_TEXCOORD0.xy * vec2(_TilingN1);
    u_xlat48.xy = u_xlat4.xy * _Splat0_ST.zz + u_xlat1.xy;
    u_xlat5 = texture(_Splat0, u_xlat48.xy);
    u_xlat48.xy = u_xlat4.xy * _Splat1_ST.zz + u_xlat1.xy;
    u_xlat6 = texture(_Splat1, u_xlat48.xy);
    u_xlat48.xy = u_xlat4.xy * _Splat2_ST.zz + u_xlat1.xy;
    u_xlat7 = texture(_Splat2, u_xlat48.xy);
    u_xlat48.xy = u_xlat4.xy * _Splat3_ST.zz + u_xlat1.xy;
    u_xlat8 = texture(_Splat3, u_xlat48.xy);
    u_xlat48.xy = u_xlat4.xy * _Splat4_STn.zz + u_xlat1.xy;
    u_xlat9 = texture(_Splat4, u_xlat48.xy);
    u_xlat48.xy = u_xlat4.xy * _Splat5_STn.zz + u_xlat1.xy;
    u_xlat10 = texture(_Splat5, u_xlat48.xy);
    u_xlat48.xy = u_xlat4.xy * _Splat6_STn.zz + u_xlat1.xy;
    u_xlat11 = texture(_Splat6, u_xlat48.xy);
    u_xlat4.xy = u_xlat4.xy * _Splat7_STn.zz + u_xlat1.xy;
    u_xlat4 = texture(_Splat7, u_xlat4.xy);
    u_xlat12.xyz = (-gs_TEXCOORD1.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat70 = dot(u_xlat12.xyz, u_xlat12.xyz);
    u_xlat70 = inversesqrt(u_xlat70);
    u_xlat12.xyz = vec3(u_xlat70) * u_xlat12.xyz;
    u_xlat70 = dot(u_xlat12.xyz, gs_TEXCOORD3.xyz);
    u_xlat70 = clamp(u_xlat70, 0.0, 1.0);
    u_xlat70 = (-u_xlat70) + 1.0;
    u_xlat70 = log2(u_xlat70);
    u_xlat70 = u_xlat70 * _RimPower;
    u_xlat12 = floor(vec4(_Metallic0, _Metallic1, _Metallic2, _Metallic3));
    u_xlat13 = floor(vec4(_Metallic4, _Metallic5, _Metallic6, _Metallic7));
    u_xlat14.xyz = u_xlat5.xyz * _Specular0.xyz;
    u_xlat15.xyz = u_xlat2.yzw * vec3(3.0, 3.0, 3.0);
    u_xlat15.xyz = clamp(u_xlat15.xyz, 0.0, 1.0);
    u_xlat15.xyz = u_xlat12.yzw * u_xlat15.xyz;
    u_xlat16.xyz = u_xlat6.xyz * _Specular1.xyz + (-u_xlat14.xyz);
    u_xlat14.xyz = u_xlat15.xxx * u_xlat16.xyz + u_xlat14.xyz;
    u_xlat16.xyz = u_xlat7.xyz * u_xlat12.zzz;
    u_xlat16.xyz = u_xlat16.xyz * _Specular2.xyz + (-u_xlat14.xyz);
    u_xlat14.xyz = u_xlat15.yyy * u_xlat16.xyz + u_xlat14.xyz;
    u_xlat15.xyw = u_xlat8.xyz * u_xlat12.www;
    u_xlat15.xyw = u_xlat15.xyw * _Specular3.xyz + (-u_xlat14.xyz);
    u_xlat14.xyz = u_xlat15.zzz * u_xlat15.xyw + u_xlat14.xyz;
    u_xlat15.xyz = u_xlat9.xyz * u_xlat13.xxx;
    u_xlat16 = u_xlat3 * vec4(3.0, 3.0, 3.0, 3.0);
    u_xlat16 = clamp(u_xlat16, 0.0, 1.0);
    u_xlat16 = u_xlat13 * u_xlat16;
    u_xlat15.xyz = u_xlat15.xyz * _Specular4.xyz + (-u_xlat14.xyz);
    u_xlat14.xyz = u_xlat16.xxx * u_xlat15.xyz + u_xlat14.xyz;
    u_xlat15.xyz = u_xlat10.xyz * u_xlat13.yyy;
    u_xlat15.xyz = u_xlat15.xyz * _Specular5.xyz + (-u_xlat14.xyz);
    u_xlat14.xyz = u_xlat16.yyy * u_xlat15.xyz + u_xlat14.xyz;
    u_xlat15.xyz = u_xlat11.xyz * u_xlat13.zzz;
    u_xlat15.xyz = u_xlat15.xyz * _Specular6.xyz + (-u_xlat14.xyz);
    u_xlat14.xyz = u_xlat16.zzz * u_xlat15.xyz + u_xlat14.xyz;
    u_xlat15.xyz = u_xlat4.xyz * u_xlat13.www;
    u_xlat15.xyz = u_xlat15.xyz * _Specular7.xyz + (-u_xlat14.xyz);
    u_xlat14.xyz = u_xlat16.www * u_xlat15.xyz + u_xlat14.xyz;
    u_xlat15.xyz = _GroundColor.xyz + _GroundColor.xyz;
    u_xlat14.xyz = u_xlat14.xyz * u_xlat15.xyz;
    u_xlat15 = texture(_NoGrassTex, u_xlat45.xy);
    u_xlat45.x = (-u_xlat2.x) * u_xlat12.x + u_xlat15.x;
    u_xlat45.x = (-u_xlat2.y) * u_xlat12.y + u_xlat45.x;
    u_xlat45.x = (-u_xlat2.z) * u_xlat12.z + u_xlat45.x;
    u_xlat45.x = (-u_xlat2.w) * u_xlat12.w + u_xlat45.x;
    u_xlat45.x = (-u_xlat3.x) * u_xlat13.x + u_xlat45.x;
    u_xlat45.x = (-u_xlat3.y) * u_xlat13.y + u_xlat45.x;
    u_xlat45.x = (-u_xlat3.z) * u_xlat13.z + u_xlat45.x;
    u_xlat45.x = (-u_xlat3.w) * u_xlat13.w + u_xlat45.x;
    u_xlat45.x = clamp(u_xlat45.x, 0.0, 1.0);
    u_xlat67 = min(_AdditionalLightsCount.x, unity_LightData.y);
    u_xlati67 = int(u_xlat67);
    u_xlatb71 = u_xlat45.x==0.0;
    if(u_xlatb71){
        u_xlatb71 = 0.0<gs_TEXCOORD2;
        if(((int(u_xlatb71) * int(0xffffffffu)))!=0){discard;}
        u_xlat71 = u_xlat70 * _RimPower;
        u_xlat71 = exp2(u_xlat71);
        u_xlat15.xyz = _RimColor.xyz * vec3(u_xlat71) + u_xlat14.xyz;
        u_xlat71 = _MainLightColor.y + _MainLightColor.x;
        u_xlat71 = u_xlat71 + _MainLightColor.z;
        u_xlatb71 = 0.0<u_xlat71;
        u_xlat16.xyz = u_xlat15.xyz * _MainLightColor.xyz;
        u_xlat15.xyz = (bool(u_xlatb71)) ? u_xlat16.xyz : u_xlat15.xyz;
        u_xlat16.xyz = u_xlat15.xyz;
        for(uint u_xlatu_loop_1 = 0u ; u_xlatu_loop_1<uint(u_xlati67) ; u_xlatu_loop_1++)
        {
            u_xlati72 = int(u_xlatu_loop_1 & 3u);
            u_xlatu73 = u_xlatu_loop_1 >> 2u;
            u_xlat72 = dot(unity_LightIndices[int(u_xlatu73)], ImmCB_0[u_xlati72]);
            u_xlati72 = int(u_xlat72);
            u_xlat17.xyz = (-gs_TEXCOORD1.xyz) * _AdditionalLightsPosition[u_xlati72].www + _AdditionalLightsPosition[u_xlati72].xyz;
            u_xlat73 = dot(u_xlat17.xyz, u_xlat17.xyz);
            u_xlat73 = max(u_xlat73, 6.10351562e-05);
            u_xlat74 = inversesqrt(u_xlat73);
            u_xlat17.xyz = vec3(u_xlat74) * u_xlat17.xyz;
            u_xlat74 = float(1.0) / u_xlat73;
            u_xlat73 = u_xlat73 * _AdditionalLightsAttenuation[u_xlati72].x;
            u_xlat73 = (-u_xlat73) * u_xlat73 + 1.0;
            u_xlat73 = max(u_xlat73, 0.0);
            u_xlat73 = u_xlat73 * u_xlat73;
            u_xlat73 = u_xlat73 * u_xlat74;
            u_xlat74 = dot(_AdditionalLightsSpotDir[u_xlati72].xyz, u_xlat17.xyz);
            u_xlat74 = u_xlat74 * _AdditionalLightsAttenuation[u_xlati72].z + _AdditionalLightsAttenuation[u_xlati72].w;
            u_xlat74 = clamp(u_xlat74, 0.0, 1.0);
            u_xlat74 = u_xlat74 * u_xlat74;
            u_xlat73 = u_xlat73 * u_xlat74;
            u_xlat17.xyz = vec3(u_xlat73) * _AdditionalLightsColor[u_xlati72].xyz;
            u_xlat17.xyz = u_xlat17.xyz * vec3(_LightIntensity);
            u_xlat17.xyz = u_xlat16.xyz * u_xlat17.xyz;
            u_xlat16.xyz = u_xlat17.xyz * vec3(7.0, 7.0, 7.0) + u_xlat16.xyz;
        }
        SV_Target0.xyz = u_xlat16.xyz;
        SV_Target0.w = 1.0;
        return;
    }
    u_xlat71 = u_xlat2.z * u_xlat7.x;
    u_xlat71 = u_xlat6.x * u_xlat2.y + u_xlat71;
    u_xlat71 = u_xlat8.x * u_xlat2.w + u_xlat71;
    u_xlat71 = u_xlat9.x * u_xlat3.x + u_xlat71;
    u_xlat71 = u_xlat10.x * u_xlat3.y + u_xlat71;
    u_xlat71 = u_xlat11.x * u_xlat3.z + u_xlat71;
    u_xlat71 = u_xlat4.x * u_xlat3.w + u_xlat71;
    u_xlat71 = u_xlat5.x * u_xlat2.x + u_xlat71;
    u_xlat72 = u_xlat2.z * _Splat2_ST.w;
    u_xlat72 = _Splat1_ST.w * u_xlat2.y + u_xlat72;
    u_xlat72 = _Splat3_ST.w * u_xlat2.w + u_xlat72;
    u_xlat72 = _Splat4_STn.w * u_xlat3.x + u_xlat72;
    u_xlat72 = _Splat5_STn.w * u_xlat3.y + u_xlat72;
    u_xlat72 = _Splat6_STn.w * u_xlat3.z + u_xlat72;
    u_xlat72 = _Splat7_STn.w * u_xlat3.w + u_xlat72;
    u_xlat72 = _Splat0_ST.w * u_xlat2.x + u_xlat72;
    u_xlat72 = u_xlat72 * _GrassThinness;
    u_xlat44 = u_xlat0.z + u_xlat45.x;
    u_xlat44 = clamp(u_xlat44, 0.0, 1.0);
    u_xlat45.x = u_xlat71 + 1.0;
    u_xlat45.x = u_xlat72 * u_xlat45.x;
    u_xlat15.xy = (-vec2(gs_TEXCOORD2)) + vec2(2.0, 1.0);
    u_xlat73 = u_xlat44 * u_xlat15.x;
    u_xlat73 = u_xlat71 * u_xlat73;
    u_xlat45.x = u_xlat45.x * u_xlat73;
    u_xlat73 = u_xlat0.x + 1.0;
    u_xlat74 = u_xlat73;
    u_xlat74 = clamp(u_xlat74, 0.0, 1.0);
    u_xlat74 = u_xlat74 * u_xlat74;
    u_xlat45.x = (-u_xlat74) * u_xlat45.x + 1.0;
    u_xlat74 = u_xlat73 * u_xlat15.y;
    u_xlat71 = u_xlat71 * u_xlat44;
    u_xlat74 = u_xlat71 * u_xlat74;
    u_xlat75 = u_xlat1.x * 5.0;
    u_xlat72 = u_xlat74 * u_xlat72 + (-u_xlat75);
    u_xlatb45 = u_xlat72>=u_xlat45.x;
    u_xlat45.x = u_xlatb45 ? 1.0 : float(0.0);
    u_xlat71 = u_xlat15.y * u_xlat71;
    u_xlat71 = u_xlat71 * _GrassThinnessIntersection;
    u_xlat72 = u_xlat0.x * u_xlat44 + 0.75;
    u_xlat72 = (-u_xlat44) * u_xlat72 + 1.0;
    u_xlat45.x = u_xlat72 * u_xlat71 + u_xlat45.x;
    u_xlatb71 = gs_TEXCOORD2>=0.00999999978;
    u_xlat66 = u_xlat66 + 1.0;
    u_xlat66 = u_xlat45.x * u_xlat66 + (-gs_TEXCOORD2);
    u_xlatb66 = u_xlat66<-0.0199999996;
    u_xlatb66 = u_xlatb66 && u_xlatb71;
    if(((int(u_xlatb66) * int(0xffffffffu)))!=0){discard;}
    u_xlat66 = exp2(u_xlat70);
    u_xlat45.x = (-_RimMin) + _RimMax;
    u_xlat66 = u_xlat66 + (-_RimMin);
    u_xlat45.x = float(1.0) / u_xlat45.x;
    u_xlat66 = u_xlat66 * u_xlat45.x;
    u_xlat66 = clamp(u_xlat66, 0.0, 1.0);
    u_xlat45.x = u_xlat66 * -2.0 + 3.0;
    u_xlat66 = u_xlat66 * u_xlat66;
    u_xlat66 = u_xlat66 * u_xlat45.x;
    u_xlat15.xy = u_xlat1.xy * vec2(0.0900000036, 0.0900000036);
    u_xlat59.xy = gs_TEXCOORD0.xy * _Splat1_ST.zz + u_xlat15.xy;
    u_xlat16 = texture(_Normal1, u_xlat59.xy);
    u_xlat16.xyz = u_xlat16.xyz * _Specular1.xyz;
    u_xlat59.xy = gs_TEXCOORD0.xy * _Splat2_ST.zz + u_xlat15.xy;
    u_xlat17 = texture(_Normal2, u_xlat59.xy);
    u_xlat17.xyz = u_xlat17.xyz * _Specular2.xyz;
    u_xlat59.xy = gs_TEXCOORD0.xy * _Splat3_ST.zz + u_xlat15.xy;
    u_xlat18 = texture(_Normal3, u_xlat59.xy);
    u_xlat18.xyz = u_xlat18.xyz * _Specular3.xyz;
    u_xlat59.xy = gs_TEXCOORD0.xy * _Splat4_STn.zz + u_xlat15.xy;
    u_xlat19 = texture(_Normal4, u_xlat59.xy);
    u_xlat19.xyz = u_xlat19.xyz * _Specular4.xyz;
    u_xlat59.xy = gs_TEXCOORD0.xy * _Splat5_STn.zz + u_xlat15.xy;
    u_xlat20 = texture(_Normal5, u_xlat59.xy);
    u_xlat20.xyz = u_xlat20.xyz * _Specular5.xyz;
    u_xlat59.xy = gs_TEXCOORD0.xy * _Splat6_STn.zz + u_xlat15.xy;
    u_xlat21 = texture(_Normal6, u_xlat59.xy);
    u_xlat21.xyz = u_xlat21.xyz * _Specular6.xyz;
    u_xlat15.xy = gs_TEXCOORD0.xy * _Splat7_STn.zz + u_xlat15.xy;
    u_xlat15 = texture(_Normal7, u_xlat15.xy);
    u_xlat15.xyz = u_xlat15.xyz * _Specular7.xyz;
    u_xlat6.xyz = u_xlat6.xyz * _Specular1.xyz + (-u_xlat16.xyz);
    u_xlat6.xyz = u_xlat12.yyy * u_xlat6.xyz + u_xlat16.xyz;
    u_xlat7.xyz = u_xlat7.xyz * _Specular2.xyz + (-u_xlat17.xyz);
    u_xlat7.xyz = u_xlat12.zzz * u_xlat7.xyz + u_xlat17.xyz;
    u_xlat8.xyz = u_xlat8.xyz * _Specular3.xyz + (-u_xlat18.xyz);
    u_xlat8.xyz = u_xlat12.www * u_xlat8.xyz + u_xlat18.xyz;
    u_xlat9.xyz = u_xlat9.xyz * _Specular4.xyz + (-u_xlat19.xyz);
    u_xlat9.xyz = u_xlat13.xxx * u_xlat9.xyz + u_xlat19.xyz;
    u_xlat10.xyz = u_xlat10.xyz * _Specular5.xyz + (-u_xlat20.xyz);
    u_xlat10.xyz = u_xlat13.yyy * u_xlat10.xyz + u_xlat20.xyz;
    u_xlat11.xyz = u_xlat11.xyz * _Specular6.xyz + (-u_xlat21.xyz);
    u_xlat11.xyz = u_xlat13.zzz * u_xlat11.xyz + u_xlat21.xyz;
    u_xlat4.xyz = u_xlat4.xyz * _Specular7.xyz + (-u_xlat15.xyz);
    u_xlat4.xyz = u_xlat13.www * u_xlat4.xyz + u_xlat15.xyz;
    u_xlat7.xyz = u_xlat2.zzz * u_xlat7.xyz;
    u_xlat6.xyz = u_xlat6.xyz * u_xlat2.yyy + u_xlat7.xyz;
    u_xlat24.xyz = u_xlat8.xyz * u_xlat2.www + u_xlat6.xyz;
    u_xlat24.xyz = u_xlat9.xyz * u_xlat3.xxx + u_xlat24.xyz;
    u_xlat24.xyz = u_xlat10.xyz * u_xlat3.yyy + u_xlat24.xyz;
    u_xlat24.xyz = u_xlat11.xyz * u_xlat3.zzz + u_xlat24.xyz;
    u_xlat24.xyz = u_xlat4.xyz * u_xlat3.www + u_xlat24.xyz;
    u_xlat2.xyz = u_xlat5.xyz * u_xlat2.xxx + u_xlat24.xyz;
    u_xlat23.xy = gs_TEXCOORD0.xy * vec2(vec2(_TilingN2, _TilingN2)) + u_xlat1.xy;
    u_xlat3 = texture(_Noise, u_xlat23.xy);
    u_xlat23.x = u_xlat3.x * _NoisePower;
    u_xlat3.xyz = _Color.xyz + _Color.xyz;
    u_xlat2.xyz = log2(u_xlat2.xyz);
    u_xlat2.xyz = u_xlat2.xyz * vec3(vec3(_GrassSaturation, _GrassSaturation, _GrassSaturation));
    u_xlat2.xyz = exp2(u_xlat2.xyz);
    u_xlat2.xyz = u_xlat2.xyz * vec3(vec3(_GrassSaturation, _GrassSaturation, _GrassSaturation));
    u_xlat2.xyz = u_xlat3.xyz * u_xlat2.xyz;
    u_xlat45.x = log2(gs_TEXCOORD2);
    u_xlat45.x = u_xlat45.x * 1.10000002;
    u_xlat45.x = exp2(u_xlat45.x);
    u_xlat4.xyz = (-_SelfShadowColor.xyz) + vec3(1.0, 1.0, 1.0);
    u_xlat4.xyz = u_xlat45.xxx * u_xlat4.xyz + _SelfShadowColor.xyz;
    u_xlat1.x = dot(u_xlat23.xx, u_xlat1.xx);
    u_xlat23.x = _GrassShading * u_xlat73 + (-u_xlat1.x);
    u_xlat4.xyz = u_xlat23.xxx + u_xlat4.xyz;
    u_xlat1.xyz = (-u_xlat1.xxx) + u_xlat4.xyz;
    u_xlat1.xyz = clamp(u_xlat1.xyz, 0.0, 1.0);
    u_xlat1.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat0.x = u_xlat0.x * -0.100000001 + 1.0;
    u_xlat2.xyz = u_xlat0.xxx * u_xlat3.xyz;
    u_xlat1.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat0.x = gs_TEXCOORD2 + -0.699999988;
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
    u_xlat0.x = (-u_xlat0.x) + 1.0;
    u_xlat0.x = (-u_xlat0.y) * u_xlat0.x + 1.0;
    u_xlat2.xyz = u_xlat0.xxx * u_xlat1.xyz;
    u_xlatb22 = 0.00999999978>=gs_TEXCOORD2;
    u_xlat1.xyz = u_xlat1.xyz * u_xlat0.xxx + (-u_xlat14.xyz);
    u_xlat1.xyz = vec3(u_xlat44) * u_xlat1.xyz + u_xlat14.xyz;
    u_xlat0.xyz = (bool(u_xlatb22)) ? u_xlat1.xyz : u_xlat2.xyz;
    u_xlat66 = log2(u_xlat66);
    u_xlat66 = u_xlat66 * _RimPower;
    u_xlat66 = exp2(u_xlat66);
    u_xlat0.xyz = _RimColor.xyz * vec3(u_xlat66) + u_xlat0.xyz;
    u_xlat66 = _MainLightColor.y + _MainLightColor.x;
    u_xlat66 = u_xlat66 + _MainLightColor.z;
    u_xlatb66 = 0.0<u_xlat66;
    u_xlat1.xyz = u_xlat0.xyz * _MainLightColor.xyz;
    u_xlat0.xyz = (bool(u_xlatb66)) ? u_xlat1.xyz : u_xlat0.xyz;
    u_xlat1.xyz = u_xlat0.xyz;
    for(uint u_xlatu_loop_2 = 0u ; u_xlatu_loop_2<uint(u_xlati67) ; u_xlatu_loop_2++)
    {
        u_xlati2 = int(u_xlatu_loop_2 & 3u);
        u_xlatu24 = u_xlatu_loop_2 >> 2u;
        u_xlat2.x = dot(unity_LightIndices[int(u_xlatu24)], ImmCB_0[u_xlati2]);
        u_xlati2 = int(u_xlat2.x);
        u_xlat24.xyz = (-gs_TEXCOORD1.xyz) * _AdditionalLightsPosition[u_xlati2].www + _AdditionalLightsPosition[u_xlati2].xyz;
        u_xlat3.x = dot(u_xlat24.xyz, u_xlat24.xyz);
        u_xlat3.x = max(u_xlat3.x, 6.10351562e-05);
        u_xlat25 = inversesqrt(u_xlat3.x);
        u_xlat24.xyz = u_xlat24.xyz * vec3(u_xlat25);
        u_xlat25 = float(1.0) / u_xlat3.x;
        u_xlat3.x = u_xlat3.x * _AdditionalLightsAttenuation[u_xlati2].x;
        u_xlat3.x = (-u_xlat3.x) * u_xlat3.x + 1.0;
        u_xlat3.x = max(u_xlat3.x, 0.0);
        u_xlat3.x = u_xlat3.x * u_xlat3.x;
        u_xlat3.x = u_xlat3.x * u_xlat25;
        u_xlat24.x = dot(_AdditionalLightsSpotDir[u_xlati2].xyz, u_xlat24.xyz);
        u_xlat24.x = u_xlat24.x * _AdditionalLightsAttenuation[u_xlati2].z + _AdditionalLightsAttenuation[u_xlati2].w;
        u_xlat24.x = clamp(u_xlat24.x, 0.0, 1.0);
        u_xlat24.x = u_xlat24.x * u_xlat24.x;
        u_xlat24.x = u_xlat24.x * u_xlat3.x;
        u_xlat2.xyz = u_xlat24.xxx * _AdditionalLightsColor[u_xlati2].xyz;
        u_xlat2.xyz = u_xlat2.xyz * vec3(_LightIntensity);
        u_xlat2.xyz = u_xlat1.xyz * u_xlat2.xyz;
        u_xlat1.xyz = u_xlat2.xyz * vec3(7.0, 7.0, 7.0) + u_xlat1.xyz;
    }
    SV_Target0.xyz = u_xlat1.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif
#ifdef GEOMETRY
#version 410
#extension GL_ARB_explicit_attrib_location : require

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	int _NumberOfStacks;
uniform 	int _MinimumNumberStacks;
uniform 	vec4 _OffsetVector;
uniform 	float _OffsetValue;
uniform 	float _GrassCut;
uniform 	float _FadeDistanceStart;
uniform 	float _FadeDistanceEnd;
UNITY_BINDING(1) uniform UnityPerDraw {
	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	vec4 unity_LODFade;
	vec4 unity_WorldTransformParams;
	vec4 unity_LightData;
	vec4 unity_LightIndices[2];
	vec4 unity_ProbesOcclusion;
	vec4 unity_SpecCube0_HDR;
	vec4 unity_LightmapST;
	vec4 unity_DynamicLightmapST;
	vec4 unity_SHAr;
	vec4 unity_SHAg;
	vec4 unity_SHAb;
	vec4 unity_SHBr;
	vec4 unity_SHBg;
	vec4 unity_SHBb;
	vec4 unity_SHC;
};
layout(location = 0) in  vec2 vs_TEXCOORD0 [3];
layout(location = 1) in  float vs_TEXCOORD5 [3];
layout(location = 2) in  vec4 vs_TEXCOORD1 [3];
layout(location = 3) in  vec3 vs_TEXCOORD2 [3];
layout(location = 4) in  vec4 vs_TEXCOORD4 [3];
float u_xlat0;
vec4 u_xlat1;
vec3 u_xlat2;
float u_xlat3;
vec4 u_xlat4;
vec4 u_xlat5;
vec4 u_xlat6;
vec3 u_xlat7;
vec3 u_xlat8;
int u_xlati8;
vec3 u_xlat10;
bool u_xlatb10;
int u_xlati11;
float u_xlat16;
bool u_xlatb16;
float u_xlat19;
bool u_xlatb19;
float u_xlat24;
int u_xlati24;
bool u_xlatb27;
layout(triangles) in;
layout(triangle_strip) out;
layout(location = 0) out vec2 gs_TEXCOORD0;
layout(location = 1) out float gs_TEXCOORD2;
layout(location = 4) out float gs_TEXCOORD5;
layout(location = 2) out vec3 gs_TEXCOORD1;
layout(location = 3) out vec3 gs_TEXCOORD3;
layout(location = 5) out vec4 gs_TEXCOORD4;
layout(max_vertices = 51) out;
void main()
{
    u_xlat0 = _OffsetValue * 0.00999999978;
    for(int u_xlati_loop_1 = 0 ; u_xlati_loop_1<3 ; u_xlati_loop_1++)
    {
        u_xlat1.x = dot(vs_TEXCOORD2[u_xlati_loop_1].xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
        u_xlat1.y = dot(vs_TEXCOORD2[u_xlati_loop_1].xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
        u_xlat1.z = dot(vs_TEXCOORD2[u_xlati_loop_1].xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
        u_xlat16 = dot(u_xlat1.xyz, u_xlat1.xyz);
        u_xlat16 = max(u_xlat16, 1.17549435e-38);
        u_xlat16 = inversesqrt(u_xlat16);
        u_xlat1.xyz = vec3(u_xlat16) * u_xlat1.xyz;
        u_xlat2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[1].xyz * vs_TEXCOORD1[u_xlati_loop_1].yyy;
        u_xlat2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * vs_TEXCOORD1[u_xlati_loop_1].xxx + u_xlat2.xyz;
        u_xlat2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * vs_TEXCOORD1[u_xlati_loop_1].zzz + u_xlat2.xyz;
        u_xlat2.xyz = u_xlat2.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
        gs_TEXCOORD0.xy = vs_TEXCOORD0[u_xlati_loop_1].xy;
        gs_TEXCOORD2 = _GrassCut;
        gs_TEXCOORD5 = 0.0;
        gl_Position = gl_in[u_xlati_loop_1].gl_Position;
        gs_TEXCOORD1.xyz = u_xlat2.xyz;
        gs_TEXCOORD3.xyz = u_xlat1.xyz;
        gs_TEXCOORD4 = vs_TEXCOORD4[u_xlati_loop_1];
        EmitVertex();
    }
    EndPrimitive();
    u_xlat8.xyz = vec3(0.333333343, 0.333333343, 0.333333343) * vs_TEXCOORD1[1].xyz;
    u_xlat8.xyz = vs_TEXCOORD1[0].xyz * vec3(0.333333343, 0.333333343, 0.333333343) + u_xlat8.xyz;
    u_xlat8.xyz = vs_TEXCOORD1[2].xyz * vec3(0.333333343, 0.333333343, 0.333333343) + u_xlat8.xyz;
    u_xlat1.xyz = u_xlat8.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * u_xlat8.xxx + u_xlat1.xyz;
    u_xlat8.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * u_xlat8.zzz + u_xlat1.xyz;
    u_xlat8.xyz = u_xlat8.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat8.xyz = (-u_xlat8.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat8.x = dot(u_xlat8.xyz, u_xlat8.xyz);
    u_xlat8.x = sqrt(u_xlat8.x);
    u_xlatb16 = 0.0<u_xlat8.x;
    u_xlati24 = _NumberOfStacks + 1;
    u_xlat24 = float(u_xlati24);
    u_xlat8.x = u_xlat8.x + (-_FadeDistanceStart);
    u_xlat1.x = (-_FadeDistanceStart) + _FadeDistanceEnd;
    u_xlat1.x = max(u_xlat1.x, 9.99999975e-05);
    u_xlat1.x = float(1.0) / u_xlat1.x;
    u_xlat8.x = u_xlat8.x * u_xlat1.x;
    u_xlat8.x = u_xlat8.x * (-u_xlat24) + u_xlat24;
    u_xlati8 = int(u_xlat8.x);
    u_xlati24 = max(_MinimumNumberStacks, 0);
    u_xlati24 = min(u_xlati24, _NumberOfStacks);
    u_xlati8 = max(u_xlati24, u_xlati8);
    u_xlati8 = min(u_xlati8, 17);
    u_xlati8 = min(u_xlati8, _NumberOfStacks);
    u_xlati8 = (u_xlatb16) ? u_xlati8 : _NumberOfStacks;
    u_xlat8.x = float(u_xlati8);
    u_xlat1 = u_xlat8.xxxx * _OffsetVector;
    u_xlat16 = _OffsetValue * 0.00999999978 + -0.00100000005;
    u_xlat24 = u_xlat8.x + (-_GrassCut);
    u_xlat2.x = 1.0;
    while(true){
        u_xlatb10 = u_xlat8.x<u_xlat2.x;
        if(u_xlatb10){break;}
        u_xlat10.xyz = u_xlat2.xxx * _OffsetVector.xyz;
        u_xlat3 = u_xlat2.x / u_xlat24;
        for(int u_xlati_loop_2 = 0 ; u_xlati_loop_2<3 ; u_xlati_loop_2++)
        {
            u_xlat4 = u_xlat1 * vec4(0.00999999978, 0.00999999978, 0.00999999978, 0.00999999978) + vs_TEXCOORD4[u_xlati_loop_2];
            u_xlat5.xyz = hlslcc_mtx4x4unity_ObjectToWorld[1].xyz * vs_TEXCOORD1[u_xlati_loop_2].yyy;
            u_xlat5.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * vs_TEXCOORD1[u_xlati_loop_2].xxx + u_xlat5.xyz;
            u_xlat5.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * vs_TEXCOORD1[u_xlati_loop_2].zzz + u_xlat5.xyz;
            u_xlat5.xyz = u_xlat5.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
            u_xlat5.xyz = (-u_xlat5.xyz) + _WorldSpaceCameraPos.xyz;
            u_xlat19 = dot(u_xlat5.xyz, u_xlat5.xyz);
            u_xlat19 = sqrt(u_xlat19);
            u_xlatb27 = 0.0<u_xlat19;
            u_xlat19 = u_xlat19 + -1.0;
            u_xlat19 = u_xlat16 * u_xlat19;
            u_xlat19 = u_xlat19 * 0.5 + 0.00100000005;
            u_xlat19 = max(u_xlat19, 0.00100000005);
            u_xlat19 = min(u_xlat0, u_xlat19);
            u_xlat19 = (u_xlatb27) ? u_xlat19 : 1.0;
            u_xlat5.xyz = vec3(u_xlat19) * vs_TEXCOORD2[u_xlati_loop_2].xyz;
            u_xlat5.xyz = u_xlat5.xyz * u_xlat2.xxx + vs_TEXCOORD1[u_xlati_loop_2].xyz;
            u_xlat5.xyz = u_xlat10.xyz * vec3(0.00999999978, 0.00999999978, 0.00999999978) + u_xlat5.xyz;
            u_xlat6.xyz = u_xlat5.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
            u_xlat5.xyw = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * u_xlat5.xxx + u_xlat6.xyz;
            u_xlat5.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * u_xlat5.zzz + u_xlat5.xyw;
            u_xlat5.xyz = u_xlat5.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
            u_xlat6 = u_xlat5.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
            u_xlat6 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat5.xxxx + u_xlat6;
            u_xlat6 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat5.zzzz + u_xlat6;
            u_xlat6 = u_xlat6 + hlslcc_mtx4x4unity_MatrixVP[3];
            u_xlat7.x = dot(vs_TEXCOORD2[u_xlati_loop_2].xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
            u_xlat7.y = dot(vs_TEXCOORD2[u_xlati_loop_2].xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
            u_xlat7.z = dot(vs_TEXCOORD2[u_xlati_loop_2].xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
            u_xlat19 = dot(u_xlat7.xyz, u_xlat7.xyz);
            u_xlat19 = max(u_xlat19, 1.17549435e-38);
            u_xlat19 = inversesqrt(u_xlat19);
            u_xlat7.xyz = vec3(u_xlat19) * u_xlat7.xyz;
            gs_TEXCOORD0.xy = vs_TEXCOORD0[u_xlati_loop_2].xy;
            gs_TEXCOORD2 = u_xlat3;
            gs_TEXCOORD5 = 0.0;
            gl_Position = u_xlat6;
            gs_TEXCOORD1.xyz = u_xlat5.xyz;
            gs_TEXCOORD3.xyz = u_xlat7.xyz;
            gs_TEXCOORD4 = u_xlat4;
            EmitVertex();
        }
        EndPrimitive();
        u_xlat2.x = u_xlat2.x + 1.0;
    }
    return;
}

#endif
                              $Globals  1      _MainLightColor                          _AdditionalLightsCount                          _Time                            _WorldSpaceCameraPos                  0   	   _Position                     D      _OrthographicCamSize                  P      _HasRT                    T   	   _TilingN1                     X   	   _TilingN2                     \   
   _WindForce                    `      _Color                    p      _SelfShadowColor                  �      _GroundColor                  �   	   _TilingN3                     �      _WindMovement                     �      _GrassThinness                    �      _GrassShading                     �      _GrassThinnessIntersection                    �   	   _RimColor                     �   	   _RimPower                     �      _NoisePower                   �      _GrassSaturation                  �      _RimMin                   �      _RimMax                   �   
   _Specular0                    �   
   _Specular1                       
   _Specular2                      
   _Specular3                       
   _Specular4                    0  
   _Specular5                    @  
   _Specular6                    P  
   _Specular7                    `  
   _Splat0_ST                    p  
   _Splat1_ST                    �  
   _Splat2_ST                    �  
   _Splat3_ST                    �     _Splat4_STn                   �     _Splat5_STn                   �     _Splat6_STn                   �     _Splat7_STn                   �  
   _Metallic0                    �  
   _Metallic1                    �  
   _Metallic2                    �  
   _Metallic3                    �  
   _Metallic4                       
   _Metallic5                      
   _Metallic6                      
   _Metallic7                         _LightIntensity                            AdditionalLights 
        _AdditionalLightsPosition                            _AdditionalLightsColor                          _AdditionalLightsAttenuation                        _AdditionalLightsSpotDir                         _AdditionalLightsOcclusionProbes                            UnityPerDraw�        unity_LODFade                     �      unity_WorldTransformParams                    �      unity_LightData                   �      unity_LightIndices                   �      unity_ProbesOcclusion                     �      unity_SpecCube0_HDR                   �      unity_LightmapST                  �      unity_DynamicLightmapST                      
   unity_SHAr                      
   unity_SHAg                       
   unity_SHAb                    0  
   unity_SHBr                    @  
   unity_SHBg                    P  
   unity_SHBb                    `  	   unity_SHC                     p     unity_ObjectToWorld                         unity_WorldToObject                  @          $GlobalsP         _MainTex_ST                   @      unity_MatrixVP                              MainLightShadows�        _CascadeShadowSplitSpheres0                   @     _CascadeShadowSplitSpheres1                   P     _CascadeShadowSplitSpheres2                   `     _CascadeShadowSplitSpheres3                   p     _CascadeShadowSplitSphereRadii                    �     _MainLightShadowOffset0                   �     _MainLightShadowOffset1                   �     _MainLightShadowOffset2                   �     _MainLightShadowOffset3                   �     _MainLightShadowParams                    �     _MainLightShadowmapSize                   �     _MainLightWorldToShadow                            $Globals�   	      _WorldSpaceCameraPos                         _NumberOfStacks                  P      _MinimumNumberStacks                 T      _OffsetVector                     `      _OffsetValue                  p   	   _GrassCut                     t      _FadeDistanceStart                    x      _FadeDistanceEnd                  |      unity_MatrixVP                                _GlobalEffectRT                	   _Control0                	   _Control1                   _NoGrassTex                 _Distortion                 _Noise                  _Splat0                 _Splat1                 _Splat2                 _Splat3     	   	         _Splat4     
   
         _Splat5                 _Splat6                 _Splat7                 _Normal1                _Normal2                _Normal3                _Normal4                _Normal5                _Normal6                _Normal7                AdditionalLights              UnityPerDraw             MainLightShadows          