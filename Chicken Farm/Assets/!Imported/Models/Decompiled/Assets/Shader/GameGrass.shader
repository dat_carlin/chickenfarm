Shader "Game/Grass" {
	Properties {
		_ColorTop ("Color Top", Vector) = (0,1,0,1)
		_ColorBottom ("Color Bottom", Vector) = (0,0.5,0,1)
		[Header(Fake Light)] _DarkTop ("Dark Top", Range(0, 1)) = 0.356
		_DarkBottom ("Dark Bottom", Range(0, 1)) = 0.133
		[Header(Grass Map)] _GrassMap ("Grass Map", 2D) = "white" {}
		[Header(Noise)] _NoiseFreq ("Noise Freq", Float) = 1
		_NoiseFreqMul ("Noise Freq Mul", Float) = 0
		_NoiseAmplitude ("Noise Amplitude", Float) = 0.2
		_Noise ("Noise", 2D) = "white" {}
		[Header(Wind)] _WindDir ("Wind Dir", Vector) = (1,1,0,0)
		_WindFreq ("Wind Freq", Float) = 1
		_WindAmplitude ("Wind Amplitude", Float) = 0.2
		_WindScale ("Wind Scale", Float) = 20
		_WindDistortion ("Wind Distortion", Float) = 0
		[Header(Bending)] _BendMap ("Bend map tex", 2D) = "grey" {}
		_BendForce ("Bend Force", Float) = 1
	}
	//DummyShaderTextExporter
	SubShader{
		Tags { "RenderType" = "Opaque" }
		LOD 200
		CGPROGRAM
#pragma surface surf Standard
#pragma target 3.0

		struct Input
		{
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			o.Albedo = 1;
		}
		ENDCG
	}
}