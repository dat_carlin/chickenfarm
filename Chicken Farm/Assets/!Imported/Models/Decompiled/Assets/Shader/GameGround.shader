Shader "Game/Ground" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_TexWeight ("Texture Weight", Range(0, 5)) = 0.5
		_TexAvg ("Texture Avgerage", Range(0, 1)) = 0.25
		_ColorTex ("Color Tex", 2D) = "white" {}
		_Dark ("Dark", Float) = 0.72627
		_Saturation ("Saturation", Range(0, 5)) = 1
		_Dir ("Dir", Vector) = (0,1,0,0)
		_Glossiness ("Smoothness", Range(0, 1)) = 0.5
		_Metallic ("Metallic", Range(0, 1)) = 0
	}
	//DummyShaderTextExporter
	SubShader{
		Tags { "RenderType"="Opaque" }
		LOD 200
		CGPROGRAM
#pragma surface surf Standard alpha
#pragma target 3.0

		sampler2D _MainTex;
		struct Input
		{
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	Fallback "Diffuse"
}