using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum ChickenMovementState
{
    None,
    RandomDirection = 10,
    Feeder = 20,
    Stopped = 30
}

public class ChickenMovement : MonoBehaviour
{
    public float wanderRadius;
    private float wanderTimer;

    [SerializeField] private float m_movementSpeed;

    private ChickenHungerManager m_hungerManager;
    private ChickenInstance m_chickenInstance;
    private Transform target;
    private NavMeshAgent agent;
    private float timer;

    private bool m_isRandomMovement;

    private Vector3 m_destinationPosition;

    private ChickenMovementState m_movementState;
    private ChickenNeedType m_needType;

    // Use this for initialization
    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        m_chickenInstance = GetComponent<ChickenInstance>();
        wanderTimer = Random.Range(2f, 7f);
        timer = wanderTimer;
    }

    private void Start()
    {
        m_hungerManager = GetComponent<ChickenHungerManager>();
        EnableRandomMovement();
        agent.speed = m_movementSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        switch (m_movementState)
        {
            case ChickenMovementState.RandomDirection:
                {
                    timer += Time.deltaTime;
                    if (timer >= wanderTimer)
                    {
                        Vector3 newPos = RandomNavSphere(transform.position, wanderRadius, -1);
                        if (agent.enabled)
                        {
                            agent.SetDestination(newPos);
                        }
                        timer = 0;
                    }
                    break;
                }
            case ChickenMovementState.Feeder:
                {
                    if (agent.enabled)
                    {
                        if (!agent.pathPending)
                        {
                            if (agent.remainingDistance <= agent.stoppingDistance)
                            {
                                if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
                                {
                                    OnReachedDestination();
                                }
                            }
                        }
                    }
                    break;
                }
        }
    }

    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }

    public void FullStop()
    {
        agent.isStopped = true;
        enabled = false;
    }

    public void StopMove()
    {
        agent.isStopped = true;
        m_chickenInstance.PlayStandStillAnimation();
    }

    public void StartMove()
    {
        agent.isStopped = false;
        m_chickenInstance.PlayRunAnimation();
    }

    public void EnableRandomMovement()
    {
        m_movementState = ChickenMovementState.RandomDirection;
        if (agent.enabled)
        {
            agent.isStopped = false;
        }
    }

    private void DisableRandomMovement()
    {
        m_movementState = ChickenMovementState.Feeder;
        if (agent.enabled)
        {
            agent.isStopped = false;
        }
    }

    public void SetFeederDestination(Vector3 feederPosition, ChickenNeedType needType)
    {
        m_needType = needType;
        m_destinationPosition = feederPosition;
        DisableRandomMovement();
        agent.SetDestination(m_destinationPosition);
    }

    private void OnReachedDestination()
    {
        switch (m_needType)
        {
            case ChickenNeedType.Food:
                {
                    StartCoroutine(m_hungerManager.StartFeedChicken());
                    break;
                }
            case ChickenNeedType.Water:
                {
                    StartCoroutine(m_hungerManager.StartWaterChicken());
                    break;
                }
        }
    }
}
