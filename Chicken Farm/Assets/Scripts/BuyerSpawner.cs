﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyerSpawner : MonoBehaviour
{
    [SerializeField] private GameObject m_buyerPrefab;
    [SerializeField] private Transform m_spawnPoint;
    [SerializeField] private Transform m_parent;

    private EggSellingStore m_eggSellingStore;

    private void Start()
    {
        m_eggSellingStore = EggSellingStore.Instance;
        StartCoroutine(SpawnBuyerWithDelay());
    }

    private IEnumerator SpawnBuyerWithDelay()
    {
        yield return new WaitForSeconds(1f);
        if (m_eggSellingStore.GetCurrentEggsAmount() > 0)
        {
            GameObject buyer = Instantiate(m_buyerPrefab);
            buyer.transform.SetParent(m_parent);
            buyer.transform.position = m_spawnPoint.position;
        }
        yield return new WaitForSeconds(Random.Range(3f, 8f));
        StartCoroutine(SpawnBuyerWithDelay());

    }
}
