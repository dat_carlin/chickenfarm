﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EggSellingStore : MonoBehaviour
{
    public static EggSellingStore Instance;
    [SerializeField] private TextMeshProUGUI m_eggAmountText;
    [Space]
    [SerializeField] private Transform m_inPatrolKit;
    [SerializeField] private Transform m_outPatrolKit;

    private int m_currentEggsAmount;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        RefreshEggsAmountText();
    }

    public Transform GetInPatrolKit()
    {
        return m_inPatrolKit;
    }

    public Transform GetOutPatrolKit()
    {
        return m_outPatrolKit;
    }

    public void IncreaseCurrentEggsAmount()
    {
        m_currentEggsAmount++;
        RefreshEggsAmountText();
    }

    public void ReduceCurrentEggsAmount(int eggsCount)
    {
        if (eggsCount <= m_currentEggsAmount)
        {
            m_currentEggsAmount -= eggsCount;
        }
        else
        {
            m_currentEggsAmount = 0;
        }

        RefreshEggsAmountText();
    }

    public int GetCurrentEggsAmount()
    {
        return m_currentEggsAmount;
    }

    private void RefreshEggsAmountText()
    {
        m_eggAmountText.text = m_currentEggsAmount.ToString();
    }
}
