using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodBucketInstance : MonoBehaviour
{
    private FoodBucketContainer m_foodContainer;

    private BoxCollider m_collider;
    private Rigidbody m_rigidbody;
    private ParabolaObjectMovement m_parabola;


    private void Awake()
    {
        m_collider = GetComponent<BoxCollider>();
        m_rigidbody = GetComponent<Rigidbody>();
        m_parabola = GetComponent<ParabolaObjectMovement>();
    }

    private void Start()
    {
        m_foodContainer = FoodBucketContainer.Instance;
    }

    public IEnumerator JumpToHeap()
    {
        if (m_foodContainer == null)
        {
            m_foodContainer = FoodBucketContainer.Instance;
        }
        Transform newParent = m_foodContainer.GetHeapFreePoint();
        if (newParent != null)
        {
            m_foodContainer.AddFoodBucket(this);
            m_collider.enabled = false;
            m_rigidbody.useGravity = false;
            m_rigidbody.isKinematic = true;
            m_parabola.JumpToPointB(newParent, newParent.position);
            //transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
            yield return new WaitForSeconds(0.25f);
            m_parabola.enabled = false;
            transform.SetParent(newParent);
            transform.localPosition = Vector3.zero;
            transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
            transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
        }
    }

    public void JumpByParabola(Transform pointB, bool defaultSettings)
    {
        if (defaultSettings)
        {
            m_parabola.JumpToPointB(pointB, pointB.position);
        }
        else
        {
            m_parabola.JumpToPointB(pointB, pointB.position, 4f, 1f);
        }
    }

    public void RemoveFoodBucket(Transform _endPoint)
    {
        transform.SetParent(null);
        m_parabola.enabled = true;
        JumpByParabola(_endPoint, true);
        gameObject.AddComponent<ObjectDestroyer>();
    }
}
