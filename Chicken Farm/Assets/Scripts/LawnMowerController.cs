using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LawnMowerController : MonoBehaviour
{
    public static LawnMowerController Instance;
    [SerializeField] private GameObject m_hayPrefab;
    [SerializeField] private Transform m_haySpawnPoint;
    private int m_currentCuttedGrainAmount;

    private void Awake()
    {
        Instance = this;
    }


    private void OnTriggerEnter(Collider other)
    {
        GrassPillarInstance grassPillar = other.GetComponent<GrassPillarInstance>();
        StartCoroutine(grassPillar.TurnOffForTime());
        IncreaseCuttedGrain();
    }


    private void IncreaseCuttedGrain()
    {
        m_currentCuttedGrainAmount++;
        CheckEnableToSpawnHay();
    }

    private void CheckEnableToSpawnHay()
    {
        if (m_currentCuttedGrainAmount % 10 == 0)
        {
            SpawnHay();
        }
    }

    private void SpawnHay()
    {
        Instantiate(m_hayPrefab, m_haySpawnPoint.position, Quaternion.identity);
    }
}
