﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EggBuyerInstance : MonoBehaviour
{

    [SerializeField] private ParticleSystem m_poofParticles;

    private Animator m_animator;
    private NavMeshAgent m_agent;

    [SerializeField] private GameObject m_thoughtImage;
    [SerializeField] private GameObject m_smileImage;
    [SerializeField] private GameObject m_sadImage;
    [SerializeField] private GameObject m_eggsBox;

    private Transform m_currentAimPoint;

    private DollarSpawner m_dollarSpawner;

    private EggSellingStore m_eggSellingStore;

    private void Awake()
    {
        m_animator = GetComponent<Animator>();
    }

    private void Start()
    {
        m_eggSellingStore = EggSellingStore.Instance;
        m_dollarSpawner = DollarSpawner.Instance;
        PlayPoofParticles();
        StartCoroutine(PlayShowAndWalkAnim());
    }

    private IEnumerator PlayShowAndWalkAnim()
    {
        PlayShowAnimation();
        yield return new WaitForSeconds(0.25f);
        SetWalkingEmptyHandsAnimation();
    }

    public void SetIdleAnimation()
    {
        m_animator.Play("Idle_EmptyHands");
    }

    public void SetWalkingEmptyHandsAnimation()
    {
        m_animator.Play("Walking_EmptyHands");
    }

    public void SetWalkingEggsBowlAnimation()
    {
        m_animator.Play("Walking_EggsBowl");
    }

    private void PlayShowAnimation()
    {
        m_animator.Play("ShowBuyer");
    }

    private void PlayHideAnimation()
    {
        m_animator.Play("HideBuyer");
    }

    private void PlayPoofParticles()
    {
        m_poofParticles.Play();
    }

    public IEnumerator DestroyWithEffects()
    {
        PlayPoofParticles();
        PlayHideAnimation();
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }

    public void MakePurchase()
    {
        m_thoughtImage.SetActive(false);
        m_smileImage.SetActive(true);
        m_eggsBox.SetActive(true);
        int randomEggsAmount = Random.Range(1, 4);
        m_eggSellingStore.ReduceCurrentEggsAmount(randomEggsAmount);
        m_dollarSpawner.SpawnDollars(transform.position + Vector3.up, randomEggsAmount * 2);
    }

    public void MakeCustomerSad()
    {
        m_thoughtImage.SetActive(false);
        m_sadImage.SetActive(true);
    }
}
