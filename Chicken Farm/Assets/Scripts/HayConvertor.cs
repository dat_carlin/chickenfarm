using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HayConvertor : MonoBehaviour
{
    public static HayConvertor Instance;

    public Transform m_endPoint;

    private HayContainer m_hayContainer;
    private GrainSupplier m_grainSupplier;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_grainSupplier = GrainSupplier.Instance;
        m_hayContainer = HayContainer.Instance;
    }

    public void StartConvertHays()
    {
        StartCoroutine(m_hayContainer.RemoveHay());
        m_grainSupplier.PlayGrainParticles();
    }
}
