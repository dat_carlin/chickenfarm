using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MergeManager : MonoBehaviour
{
    public static MergeManager Instance;

    private ChickenSpawner m_chickenSpawner;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_chickenSpawner = ChickenSpawner.Instance;
    }


    public void MergeChickens(ChickenInstance prevChick01, ChickenInstance prevChick02)
    {
        ChickensLevel prevChickLevel = prevChick01.GetChickenLevel();

        int index = Array.IndexOf(ChickensLevel.GetValues(prevChickLevel.GetType()), prevChickLevel);
        var myEnumMemberCount = Enum.GetNames(typeof(ChickensLevel)).Length;

        if (index + 1 <= myEnumMemberCount - 1)
        {
            prevChick01.TurnOffBoxCollider();
            prevChick02.TurnOffBoxCollider();
            ChickensLevel nextChickLevel = (ChickensLevel)Enum.GetValues(prevChickLevel.GetType()).GetValue(index + 1);
            m_chickenSpawner.SpawnChicken(nextChickLevel, prevChick02.transform.position);
            prevChick01.DecreaseAndDestroy();
            prevChick02.DecreaseAndDestroy();
        }
        else
        {
            StartCoroutine(prevChick01.TurnOnPhysics());
            StartCoroutine(prevChick02.TurnOnPhysics());
        }
    }
}
