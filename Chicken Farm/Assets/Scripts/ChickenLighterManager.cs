﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenLighterManager : MonoBehaviour
{
    public static ChickenLighterManager Instance;

    private Animator m_animator;

    private void Awake()
    {
        Instance = this;
        m_animator = GetComponent<Animator>();
    }

    public void ActivateCircle(Vector3 chickenPosition)
    {
        transform.position = new Vector3(chickenPosition.x, 0f, chickenPosition.z);
        m_animator.Play("ShowLighterCircle");
    }
}
