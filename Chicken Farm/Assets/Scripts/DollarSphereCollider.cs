﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DollarSphereCollider : MonoBehaviour
{
    [SerializeField] DollarInstance m_dollarInstance;
    private Collider m_collider;

    private void Awake()
    {
        m_collider = GetComponent<Collider>();
    }

    public void PutMoney()
    {
        m_dollarInstance.AddToBank();
    }
}
