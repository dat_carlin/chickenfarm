﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabbingManager : MonoBehaviour
{
    public static GrabbingManager Instance;
    [SerializeField] private CooldownCircle m_cooldownCircle;

    private bool m_isMouseActive;

    [SerializeField] private ChickenInstance m_currentChicken;
    private PlayerInstance m_playerInstance;

    private bool m_chickenInHands;
    private bool m_enableTap;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_playerInstance = PlayerInstance.Instance;
    }


    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            SetMouseActive();
            //ResetCircle();
            StartCoroutine(StartTap());
        }
        if (Input.GetMouseButtonUp(0))
        {
            SetMouseDeactive();
            if (m_currentChicken != null && !m_chickenInHands)
            {
                StartCooldownLastChicken();
            }
            if (m_enableTap)
            {
                CheckEnableToThrowChicken();
            }
        }
    }

    private IEnumerator StartTap()
    {
        m_enableTap = true;
        yield return new WaitForSeconds(0.2f);
        m_enableTap = false;
    }

    private void CheckEnableToThrowChicken()
    {
        if (m_chickenInHands)
        {
            StartCoroutine(m_playerInstance.ThrowChickenFromHands(m_currentChicken));
            SetChickenOutHands();
            FullResetCircle();
        }
    }

    public void StartCooldown(ChickenInstance _chicken)
    {
        m_currentChicken = _chicken;
        if (!m_isMouseActive)
        {
            StartCooldownLastChicken();
        }
    }

    private void StartCooldownLastChicken()
    {
        transform.position = m_currentChicken.transform.position;
        m_cooldownCircle.StartTimerVoid(m_currentChicken);
    }

    public void SimpleResetCircle()
    {
        m_cooldownCircle.StopCooling();
    }

    public void FullResetCircle()
    {
        m_cooldownCircle.StopCooling();
        if (!m_chickenInHands)
        {
            m_currentChicken = null;
        }
    }

    private void SetMouseActive()
    {
        m_isMouseActive = true;
    }

    private void SetMouseDeactive()
    {
        m_isMouseActive = false;
    }

    public void SetChickenInHands()
    {
        m_chickenInHands = true;
    }

    public void SetChickenOutHands()
    {
        m_chickenInHands = false;
    }
}
