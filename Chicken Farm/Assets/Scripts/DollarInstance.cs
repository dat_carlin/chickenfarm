﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DollarInstance : MonoBehaviour
{
    [SerializeField] private int m_price;
    private MoneyBankManager m_bank;

    private Animator m_animator;
    private Rigidbody m_rigidbody;
    private BoxCollider m_boxCollider;

    private void Awake()
    {
        m_animator = GetComponent<Animator>();
        m_rigidbody = GetComponent<Rigidbody>();
        m_boxCollider = GetComponent<BoxCollider>();
        m_coinHolder = GameObject.FindGameObjectWithTag("CoinHolderUI").gameObject.transform; //найти в UI холдер

    }

    private void Start()
    {
        m_bank = MoneyBankManager.Instance;
        PlayShowAnimation();
        RandomizeRotation();
        ForceInRandomDirection();
    }


    public void AddToBank()
    {
        m_boxCollider.enabled = false;
        m_rigidbody.useGravity = false;
        m_rigidbody.isKinematic = true;
        m_canFlyToHolder = true;
    }

    private void RandomizeRotation()
    {
        transform.rotation = Random.rotation;
    }

    private void PlayShowAnimation()
    {
        m_animator.Play("ShowDollar");
    }

    private Transform m_coinHolder;
    private bool m_canFlyToHolder;

    private float m_flyingSpeed = 50f; //40 - low, 160 - high speed
    private float m_destroyScale = 40f; //2 - low, 4 - high speed

    private void Update()
    {
        if (m_canFlyToHolder)
        {
            Vector3 screenPoint = m_coinHolder.position + new Vector3(0, 0, 5);  //the "+ new Vector3(0,0,5)" ensures that the object is so close to the camera you dont see it

            Vector3 worldPos = Camera.main.ScreenToWorldPoint(screenPoint);

            transform.position = Vector3.MoveTowards(transform.position, worldPos, m_flyingSpeed * Time.deltaTime);
            transform.localScale -= Vector3.one / m_destroyScale;

            if (transform.localScale.x < 0.01f)
            {
                m_bank.AddMoney(m_price);
                Destroy(gameObject);
            }
        }
    }

    private void ForceInRandomDirection()
    {
        m_rigidbody.AddForce(new Vector3(Random.Range(-1f, 1f), 2f, Random.Range(-1f, 1f)) * 100f);
    }
}

