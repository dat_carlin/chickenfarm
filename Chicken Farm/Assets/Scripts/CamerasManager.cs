﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamerasManager : MonoBehaviour
{
    public static CamerasManager Instance;

    [SerializeField] private GameObject m_mainCamera;
    [SerializeField] private GameObject m_shopCamera;

    private float m_swipeTime = 0.15f;

    private void Awake()
    {
        Instance = this;
    }

    public IEnumerator TurnOnShopCamera()
    {
        yield return new WaitForSeconds(m_swipeTime);
        m_shopCamera.SetActive(true);
        m_mainCamera.SetActive(false);
    }

    public IEnumerator TurnOnManCamera()
    {
        yield return new WaitForSeconds(m_swipeTime);
        m_mainCamera.SetActive(true);
        m_shopCamera.SetActive(false);
    }
}
