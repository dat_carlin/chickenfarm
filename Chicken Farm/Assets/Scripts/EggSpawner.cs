using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggSpawner : MonoBehaviour
{
    public static EggSpawner Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void SpawnEgg(GameObject eggPrefab, Vector3 spawnPosition)
    {
        Instantiate(eggPrefab, spawnPosition, Quaternion.identity);
    }
}
