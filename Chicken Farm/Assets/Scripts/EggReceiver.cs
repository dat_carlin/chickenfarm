using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggReceiver : MonoBehaviour
{
    public static EggReceiver Instance;

    [SerializeField] private Animator m_eggHouseAnimator;

    public Transform m_endPoint;

    private EggContainer m_eggContainer;
    private MoneyBankManager m_moneyBank;
    private PlayerMovement m_playerMovement;

    private EggSellingStore m_eggSellingStore;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_playerMovement = PlayerMovement.Instance;
        m_eggContainer = EggContainer.Instance;
        m_moneyBank = MoneyBankManager.Instance;
        m_eggSellingStore = EggSellingStore.Instance;
    }

    public void StartSellingEggs()
    {
        StartCoroutine(m_eggContainer.SellAllEggs());
        PlayEggHOuseAnimator();
    }

    public void SellEgg(int eggPrice)
    {
        m_moneyBank.AddMoney(eggPrice);
    }

    private void PlayEggHOuseAnimator()
    {
        m_eggHouseAnimator.Play("SwingBuilding");
    }
}
