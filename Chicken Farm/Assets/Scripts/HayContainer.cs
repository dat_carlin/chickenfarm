using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HayContainer : MonoBehaviour
{
    public static HayContainer Instance;
    [SerializeField] List<Transform> m_heapPosition = new List<Transform>();

    private List<HayInstance> m_hays = new List<HayInstance>();

    private GrainSupplier m_grainSupplier;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_grainSupplier = GrainSupplier.Instance;
    }

    public Transform GetHeapFreePoint()
    {
        Transform freePoint = null;
        if (m_heapPosition.Count > 0)
        {
            freePoint = m_heapPosition[0];
            m_heapPosition.RemoveAt(0);
        }
        return freePoint;
    }


    public void AddHay(HayInstance newHay)
    {
        m_hays.Add(newHay);
    }

    public IEnumerator RemoveHay()
    {
        if (m_hays.Count > 0)
        {
            m_heapPosition.Insert(0, m_hays[m_hays.Count - 1].transform.parent);
            m_hays[m_hays.Count - 1].RemoveHay();
            m_hays.RemoveAt(m_hays.Count - 1);
            m_grainSupplier.AddAmount();
            yield return new WaitForSeconds(0.02f);
            StartCoroutine(RemoveHay());
        }
    }
}
