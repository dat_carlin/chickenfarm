using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum ChickensLevel
{
    None = 0,
    ChickenLevel00 = 10,
    ChickenLevel01,
    ChickenLevel02,
    ChickenLevel03,
    ChickenLevel04,
    ChickenLevel05,
    ChickenLevel06,
    ChickenLevel07
}

public class ChickenInstance : MonoBehaviour
{
    [SerializeField] private ChickensLevel m_chickenLevel;
    [SerializeField] private GameObject m_checkingSphere;
    [SerializeField] private Animator m_innerChickenAnimator;
    [SerializeField] private GameObject m_eggPrefab;
    public GameObject m_spawnFeatherPrefab;


    private ParabolaObjectMovement m_parabolaController;
    private NavMeshAgent m_navMeshAgent;
    private MergeManager m_mergeManager;
    private Rigidbody m_rigidbody;
    private Animator m_animator;
    [HideInInspector] public ChickenMovement m_chickenMovement;

    private PseudoFOV m_pseudoFOV;
    private ChickenLighterManager m_chiclenLighher;
    private ChickensHolder m_chickenHolder;
    private EggSpawner m_eggSpawner;
    private FeederPoint m_feederPoint;
    private FeederInstance m_feederInstance;
    private BoxCollider m_boxCollider;

    private Coroutine m_spawnEggRoutine;

    private float m_throwForce = 250f;

    private bool m_isFlying;

    private void Awake()
    {
        m_chickenMovement = GetComponent<ChickenMovement>();
        m_boxCollider = GetComponent<BoxCollider>();
        m_animator = GetComponent<Animator>();
        m_navMeshAgent = GetComponent<NavMeshAgent>();
        m_rigidbody = GetComponent<Rigidbody>();
        m_parabolaController = GetComponent<ParabolaObjectMovement>();
    }

    private void Start()
    {
        m_eggSpawner = EggSpawner.Instance;
        m_mergeManager = MergeManager.Instance;
        m_chickenHolder = ChickensHolder.Instance;
        m_chiclenLighher = ChickenLighterManager.Instance;
        m_pseudoFOV = PseudoFOV.Instance;
        SetParentHolder();

        if (m_innerChickenAnimator != null)
        {
            PlayRunAnimation();
        }
    }

    public bool IsFlying()
    {
        return m_isFlying;
    }

    private void SetParentHolder()
    {
        transform.SetParent(m_chickenHolder.transform);
    }

    private void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.layer)
        {
            case 8: //chicken
                {
                    if (m_isFlying)
                    {
                        ChickenInstance anotherChicken = collision.gameObject.GetComponent<ChickenInstance>();

                        ChickensLevel anotherChickenLevel = anotherChicken.m_chickenLevel;

                        if (anotherChickenLevel == m_chickenLevel)
                        {
                            TurnOffBoxCollider();
                            anotherChicken.TurnOffBoxCollider();

                            m_pseudoFOV.RemoveChickenFromList(transform);
                            m_pseudoFOV.RemoveChickenFromList(anotherChicken.transform);
                            m_mergeManager.MergeChickens(this, anotherChicken);
                        }
                    }
                    break;
                }
            case 15: //death zone
                {
                    DestroyChicken();
                    break;
                }
        }
    }

    public void TurnOffBoxCollider()
    {
        m_boxCollider.enabled = false;
    }

    public ChickensLevel GetChickenLevel()
    {
        return m_chickenLevel;
    }

    public void SetChickenLevel(ChickensLevel newChickenLevel)
    {
        m_chickenLevel = newChickenLevel;
    }

    public void JumpToPlayerHands(Transform _pointB, Vector3 _endPoint)
    {
        m_parabolaController.enabled = true;
        m_parabolaController.JumpToPointB(_pointB, _endPoint);
        transform.SetParent(_pointB);
        TurnOffPhysics();
        StartCoroutine(ResetTransform());

        if (m_innerChickenAnimator != null)
        {
            PlayFlyingAnimation();
        }
    }

    private IEnumerator ResetTransform()
    {
        yield return new WaitForSeconds(0.25f);
        transform.localPosition = Vector3.zero;
        transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
    }

    public void StartFlightForward(Transform root)
    {
        StartCoroutine(TurnOnPhysics());
        Vector3 v3Force = m_throwForce * (root.forward + Vector3.up);
        m_rigidbody.AddForce(v3Force);
        transform.SetParent(null);

        if (m_innerChickenAnimator != null)
        {
            PlayFlyingAnimation();
        }
    }

    public void StartFlightToChicken(Transform anotherChicken)
    {
        ChickenInstance anotherChickenInstance = anotherChicken.GetComponent<ChickenInstance>();

        if (anotherChickenInstance.GetChickenLevel() == m_chickenLevel)
        {
            anotherChickenInstance.m_chickenMovement.FullStop();
            m_isFlying = true;
            m_rigidbody.isKinematic = false;
            transform.SetParent(null);
            float distance = Vector3.Distance(transform.position, anotherChicken.position);
            m_parabolaController.enabled = true;
            m_parabolaController.JumpToPointB(anotherChicken, anotherChicken.position, 1.5f, distance / 2f);
            //Vector3 lookDir = anotherChicken.position - transform.position;
            transform.LookAt(anotherChicken);
            m_chiclenLighher.ActivateCircle(anotherChicken.position);
            //transform.eulerAngles = new Vector3(0f, lookDir.y, 0f);
        }
        else
        {
            StartFlightForward(PlayerInstance.Instance.transform);
        }
    }

    private void TurnOffPhysics()
    {
        m_navMeshAgent.enabled = false;
        m_rigidbody.isKinematic = true;
        m_rigidbody.useGravity = false;
        m_checkingSphere.SetActive(false);
    }

    public IEnumerator TurnOnPhysics()
    {
        m_parabolaController.enabled = false;
        m_isFlying = true;
        m_rigidbody.useGravity = true;
        m_rigidbody.isKinematic = false;
        yield return new WaitForSeconds(1.5f);
        m_chickenMovement.enabled = true;
        m_chickenMovement.EnableRandomMovement();
        m_navMeshAgent.enabled = true;
        m_rigidbody.isKinematic = true;
        m_checkingSphere.SetActive(true);
        m_isFlying = false;
        SetParentHolder();

        if (m_innerChickenAnimator != null)
        {
            PlayRunAnimation();
        }
    }

    public void DecreaseAndDestroy()
    {
        m_animator.Play("DecreaseChicken");
    }

    public void PlayShowAnimation()
    {
        m_animator.Play("ShowChicken");
    }

    public void DestroyChicken()
    {
        Destroy(gameObject); //Далее отправлять в пул
    }

    public void SpawnEgg()
    {
        if (CheckChickenOnPasture() && m_chickenLevel != ChickensLevel.ChickenLevel00 && !m_isFlying)
        {
            m_eggSpawner.SpawnEgg(m_eggPrefab, transform.position - (transform.forward / 10f));
        }
    }

    public void GoToNearestFoodFeeder()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 100f);
        Collider tMin = null;
        m_feederInstance = null;
        float minDist = Mathf.Infinity;
        Vector3 currentPos = transform.position;

        foreach (Collider t in hitColliders)
        {
            if (t.gameObject.layer == 12)
            {
                FeederInstance feeder = t.gameObject.GetComponent<FeederInstance>();
                if (feeder.GetFeederType() == FeederType.Food)
                {

                    float dist = Vector3.Distance(t.transform.position, currentPos);
                    if (dist < minDist)
                    {
                        tMin = t;
                        minDist = dist;
                        m_feederInstance = t.gameObject.GetComponent<FeederInstance>();
                    }
                }
            }
        }
        m_feederPoint = m_feederInstance.GetFreeFeederPoint();
        if (m_feederPoint && m_feederInstance.IsFeederFilled())
        {
            m_chickenMovement.SetFeederDestination(m_feederPoint.transform.position, ChickenNeedType.Food);
        }
        else
        {
            m_feederInstance.AddChickenToQueueFood(this);
        }
    }

    public void GoToNearestFoodFeeder(FeederPoint newFeederPoint)
    {
        m_chickenMovement.SetFeederDestination(newFeederPoint.transform.position, ChickenNeedType.Food);
    }

    public void GoToNearestWaterFeeder()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 20f);
        Collider tMin = null;
        m_feederInstance = null;
        float minDist = Mathf.Infinity;
        Vector3 currentPos = transform.position;

        foreach (Collider t in hitColliders)
        {
            if (t.gameObject.layer == 12)
            {
                FeederInstance feeder = t.gameObject.GetComponent<FeederInstance>();
                if (feeder.GetFeederType() == FeederType.Water)
                {
                    float dist = Vector3.Distance(t.transform.position, currentPos);
                    if (dist < minDist)
                    {
                        tMin = t;
                        minDist = dist;
                        m_feederInstance = t.gameObject.GetComponent<FeederInstance>();
                    }
                }
            }
        }
        FeederPoint feederPoint = m_feederInstance.GetFreeFeederPoint();
        if (feederPoint)
        {
            m_chickenMovement.SetFeederDestination(feederPoint.transform.position, ChickenNeedType.Water);
        }
        else
        {
            m_feederInstance.AddChickenToQueueWater(this);
        }
    }

    public void GoToNearestWaterFeeder(FeederPoint newFeederPoint)
    {
        m_chickenMovement.SetFeederDestination(newFeederPoint.transform.position, ChickenNeedType.Water);
    }

    public void TurnOnMovement()
    {
        m_chickenMovement.enabled = true;
    }

    public void TurnOffMovement()
    {
        m_chickenMovement.enabled = false;
    }

    #region Inner Animator
    public void PlayRunAnimation()
    {
        m_innerChickenAnimator.Play("RunOnGround");
    }


    public void PlayFlyingAnimation()
    {
        m_innerChickenAnimator.Play("Flying");
    }

    public void PlayStandStillAnimation()
    {
        m_innerChickenAnimator.Play("StandStill");
    }
    #endregion

    public IEnumerator PlayFeedAnimation()
    {
        m_innerChickenAnimator.Play("Eating");
        yield return new WaitForSeconds(0.1f);
        m_feederInstance.ReduceGrainSize(this);
        yield return new WaitForSeconds(0.9f);
        PlayRunAnimation();
    }

    private bool CheckChickenOnPasture()
    {
        return Physics.Raycast(transform.position, Vector3.down, 10f, 1 << 30);
    }
}
