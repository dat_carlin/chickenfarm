using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenSpawner : MonoBehaviour
{
    //Спавнить по имени(из enum) из папки Resources
    public static ChickenSpawner Instance;

    private FeatherSpawner m_featherSpawner;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_featherSpawner = FeatherSpawner.Instance;
    }

    public void SpawnChicken(ChickensLevel chickenLevel, Vector3 spawnPosition)
    {
        string chickenLevelName = chickenLevel.ToString();
        GameObject newChicken = Instantiate(Resources.Load("ChickensPrefabs/" + chickenLevelName) as GameObject, spawnPosition, Quaternion.identity);
        newChicken.transform.eulerAngles = new Vector3(0f, Random.Range(0f, 360f), 0f);
        ChickenInstance chicken = newChicken.GetComponent<ChickenInstance>();
        chicken.SetChickenLevel(chickenLevel);
        chicken.PlayShowAnimation();
        m_featherSpawner.SpawnFeather(chicken.transform.position, chicken.m_spawnFeatherPrefab);
    }
}
