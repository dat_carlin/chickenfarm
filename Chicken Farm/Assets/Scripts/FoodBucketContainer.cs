using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodBucketContainer : MonoBehaviour
{
    public static FoodBucketContainer Instance;
    [SerializeField] private List<Transform> m_heapPosition = new List<Transform>();

    private List<FoodBucketInstance> m_foodBuckets = new List<FoodBucketInstance>();

    private PlayerMovement m_playerMovement;

    private int m_maxFreePoints;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_playerMovement = PlayerMovement.Instance;
        m_maxFreePoints = m_heapPosition.Count;
    }

    public Transform GetHeapFreePoint()
    {
        Transform freePoint = null;
        if (m_heapPosition.Count > 0)
        {
            freePoint = m_heapPosition[0];
            m_heapPosition.RemoveAt(0);
            CheckHandsUpAbillity();
        }
        return freePoint;
    }

    public void AddFoodBucket(FoodBucketInstance newBucket)
    {
        m_foodBuckets.Add(newBucket);
    }

    public IEnumerator RemoveBuckets(Transform endPoint, int requestedAmount, FeederInstance feeder)
    {
        if (requestedAmount > 0)
        {
            m_heapPosition.Insert(0, m_foodBuckets[m_foodBuckets.Count - 1].transform.parent);
            m_foodBuckets[m_foodBuckets.Count - 1].RemoveFoodBucket(endPoint);
            m_foodBuckets.RemoveAt(m_foodBuckets.Count - 1);
            requestedAmount--;
            feeder.AddGrainSize();
            yield return new WaitForSeconds(0.1f);
            StartCoroutine(RemoveBuckets(endPoint, requestedAmount, feeder));
        }
        else
        {
            CheckHandsDownAbillity();
        }
    }

    private void CheckHandsUpAbillity()
    {
        if (m_foodBuckets.Count == 0)
        {
            StartCoroutine(m_playerMovement.SetFoodBucketInHands());
        }
    }

    private void CheckHandsDownAbillity()
    {
        if (m_foodBuckets.Count <= 0)
        {
            StartCoroutine(m_playerMovement.SetEmptyHands());
        }
    }

    public int GetFreePointsCount()
    {
        return m_heapPosition.Count;
    }

    public int GetBusyPointsCount()
    {
        return m_maxFreePoints - m_heapPosition.Count;
    }
}
