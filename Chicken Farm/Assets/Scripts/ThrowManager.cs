using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowManager : MonoBehaviour
{
    public static ThrowManager Instance;

    [SerializeField] private GameObject m_throwButtonObject;

    private PlayerInstance m_playerInstance;

    private ChickenInstance m_currentChicken;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_playerInstance = PlayerInstance.Instance;
        TurnOffButton();
    }

    public void TurnOnButton(ChickenInstance newChicken)
    {
        m_currentChicken = newChicken;
        m_throwButtonObject.SetActive(true);
    }

    public void TurnOffButton()
    {
        m_throwButtonObject.SetActive(false);
    }

    public void OnThrowButtonClick()
    {
        StartCoroutine(m_playerInstance.ThrowChickenFromHands(m_currentChicken));
        TurnOffButton();
        m_currentChicken = null;
    }
}
