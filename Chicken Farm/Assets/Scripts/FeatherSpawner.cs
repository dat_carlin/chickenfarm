using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeatherSpawner : MonoBehaviour
{
    public static FeatherSpawner Instance;

    //[SerializeField] private ParticleSystem m_featherParticles;

    private void Awake()
    {
        Instance = this;
    }

    public void SpawnFeather(Vector3 position, GameObject particle)
    {
        GameObject particles = Instantiate(particle, position, Quaternion.identity);
        particles.AddComponent<ObjectDestroyer>();
        //m_featherParticles.transform.position = position + (Vector3.up / 2f);
        //m_featherParticles.Play();
    }
}
