﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    public static ShopManager Instance;
    public Transform m_spawnPosition;
    [SerializeField] private Button m_closeButton;
    [SerializeField] private Animator m_UIAnimator;
    [Space]
    [SerializeField] private ShopItemInstance m_shopItemPrefab;
    [SerializeField] private List<ChickenShopItem> m_shopSO = new List<ChickenShopItem>();
    [SerializeField] private RectTransform[] m_shopCells;
    private Joystick m_joystick;

    private CamerasManager m_cameraManager;

    private List<ShopItemInstance> m_shopItems = new List<ShopItemInstance>();

    private void Awake()
    {
        m_closeButton.onClick.AddListener(DeactivateShop);
        Instance = this;
    }

    private void Start()
    {
        m_cameraManager = CamerasManager.Instance;
        m_joystick = Joystick.Instance;
        FillAndSetupCells();
    }

    private void FillAndSetupCells()
    {
        for (int i = 0; i < m_shopSO.Count; i++)
        {
            ShopItemInstance shopItemInstance = Instantiate(m_shopItemPrefab, m_shopCells[i]);
            shopItemInstance.SetupShopItem(m_shopSO[i]);
            m_shopItems.Add(shopItemInstance);
        }
    }

    public void CheckBuyingAbility()
    {
        for (int i = 0; i < m_shopItems.Count; i++)
        {
            m_shopItems[i].CheckBuyingAbility();
        }
    }


    public void DisableBuying()
    {
        for (int i = 0; i < m_shopItems.Count; i++)
        {
            m_shopItems[i].DisableBuyButton();
        }
    }

    public void EnableBuying()
    {
        for (int i = 0; i < m_shopItems.Count; i++)
        {
            m_shopItems[i].EnableBuyButton();
        }
    }

    public void ActivateShop()
    {
        CheckBuyingAbility();
        m_UIAnimator.Play("ShowShop");
        StartCoroutine(m_cameraManager.TurnOnShopCamera());
        m_joystick.MouseUp();
    }

    public void DeactivateShop()
    {
        m_UIAnimator.Play("HideShop");
        StartCoroutine(m_cameraManager.TurnOnManCamera());
    }
}
