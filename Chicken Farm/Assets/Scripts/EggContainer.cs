using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggContainer : MonoBehaviour
{
    public static EggContainer Instance;

    [SerializeField] private List<Transform> m_heapPositions = new List<Transform>();
    [SerializeField] private Animator m_bowlOnBackAnimator, m_bowlInHandAnimator;


    private List<EggInstance> m_eggs = new List<EggInstance>();
    private PlayerMovement m_playerMovement;
    private Coroutine m_sellAllEggsRoutine;
    private EggSellingStore m_eggSellingStore;

    private const string m_showBowlAnimName = "ShowEggsBowl";
    private const string m_hideBowlAnimName = "HideEggsBowl";

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_playerMovement = PlayerMovement.Instance;
        m_eggSellingStore = EggSellingStore.Instance;
    }

    public Transform GetHeapFreePoint()
    {
        Transform freePoint = null;
        if (m_heapPositions.Count > 0)
        {
            freePoint = m_heapPositions[0];
            m_heapPositions.RemoveAt(0);
        }
        return freePoint;
    }

    public void AddEgg(EggInstance newEgg)
    {
        m_eggs.Add(newEgg);
        if (m_eggs.Count == 1)
        {
            ShowBowl();
            //анимация появления тарелки на спине 
        }
    }

    private void ShowBowl()
    {
        m_bowlOnBackAnimator.Play(m_hideBowlAnimName);
        m_bowlInHandAnimator.Play(m_showBowlAnimName);
        StartCoroutine(m_playerMovement.SetEggsBowlInHands());
    }

    private void HideBowl()
    {
        m_bowlInHandAnimator.Play(m_hideBowlAnimName);
        m_bowlOnBackAnimator.Play(m_showBowlAnimName);
        StartCoroutine(m_playerMovement.SetEmptyHands());
    }

    public IEnumerator SellAllEggs()
    {
        if (m_eggs.Count > 0)
        {
            m_heapPositions.Insert(0, m_eggs[m_eggs.Count - 1].transform.parent);
            m_eggs[m_eggs.Count - 1].SellEgg();
            m_eggs.RemoveAt(m_eggs.Count - 1);
            m_eggSellingStore.IncreaseCurrentEggsAmount();
            yield return new WaitForSeconds(0.02f);
            StartCoroutine(SellAllEggs());
        }
        else
        {
            HideBowl();
            //анимация возврата тарелки за спину
        }
    }

    public void HardReset()
    {
        if (m_eggs.Count > 0)
        {
            for (int i = 0; i < m_eggs.Count; i++)
            {
                Destroy(m_eggs[i]);
            }
            m_eggs.Clear();
            m_bowlInHandAnimator.Play(m_hideBowlAnimName);
            m_bowlOnBackAnimator.Play(m_showBowlAnimName);
        }
    }
}
