using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyBankManager : MonoBehaviour
{
    public static MoneyBankManager Instance;

    [SerializeField] private Text m_moneyAmountText;

    [SerializeField] private int m_moneyAmount;

    private ShopManager m_shopManager;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_shopManager = ShopManager.Instance;
        RefreshCounter();
    }

    public void AddMoney(int eggPrice)
    {
        m_moneyAmount += eggPrice;
        RefreshCounter();
    }

    public void ReduceMoney(int removingPrice)
    {
        m_moneyAmount -= removingPrice;
        RefreshCounter();
        m_shopManager.CheckBuyingAbility();
    }

    private void RefreshCounter()
    {
        m_moneyAmountText.text = m_moneyAmount.ToString();
    }

    public bool IsMoreThenBank(int checkingValue)
    {
        bool isMore = false;
        if (checkingValue > m_moneyAmount)
        {
            isMore = true;
        }
        return isMore;
    }
}
