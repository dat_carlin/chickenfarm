using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewCanvasController : MonoBehaviour
{
    private Canvas m_canvas;

    private void Awake()
    {
        m_canvas = GetComponent<Canvas>();
    }

    private IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        SetupCanvasScale();
    }

    private void SetupCanvasScale()
    {
        float screenX = Screen.width;
        float newScale = screenX / 100f / 12.42f;
        m_canvas.scaleFactor = newScale;
    }
}
