using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Joystick : MonoBehaviour
{
    public static Joystick Instance;
    [Header("Player Settings")]
    [SerializeField] private Transform m_playerCharacter;
    [SerializeField] private PlayerMovement m_player;

    [Header("Joystick Settings")]
    [SerializeField] private RectTransform m_pad;

    private CameraZoomer m_cameraZoomer;

    private Vector3 m_move;
    private float m_moveSpeed = 4f;
    private float m_rotationSpeed = 7f;
    private bool m_walking;

    [HideInInspector] public bool m_isTouched;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_cameraZoomer = CameraZoomer.Instance;
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0)) //down
        {
            MouseDown();
        }

        if (Input.GetMouseButton(0)) //drag
        {
            MouseDrag();
        }

        if (Input.GetMouseButtonUp(0)) //up
        {
            MouseUp();
        }
#endif


#if UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    {
                        MouseDown();
                        break;
                    }
                case TouchPhase.Moved:
                    {
                        MouseDrag();
                        break;
                    }
                case TouchPhase.Ended:
                    {
                        MouseUp();
                        break;
                    }
            }
        }
#endif

    }

    public void SetLawnMowerSpeed()
    {
        m_moveSpeed = 2.5f;
        m_rotationSpeed = 3f;
    }

    public void SetNormalSpeed()
    {
        m_moveSpeed = 4f;
        m_rotationSpeed = 7f;
    }

    private void MouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            m_isTouched = true;
            m_pad.position = Input.mousePosition;
            StartCoroutine("PlayerMove");
            m_cameraZoomer.StartZoomOut();
        }
    }

    private void MouseDrag()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            Vector2 eventDataPosition = Input.mousePosition;
            transform.position = eventDataPosition;
            transform.localPosition = Vector2.ClampMagnitude(eventDataPosition - (Vector2)m_pad.position, m_pad.rect.width * 0.5f);

            m_move = new Vector3(transform.localPosition.x, 0f, transform.localPosition.y).normalized;


            if (!m_walking)
            {
                m_walking = true;
                m_player.StartWalkingAnimation();
            }
        }
    }

    public void MouseUp()
    {
        m_isTouched = false;
        transform.localPosition = Vector3.zero;
        m_move = Vector3.zero;
        StopCoroutine("PlayerMove");
        m_walking = false;
        m_player.StopWalkingAnimation();
        m_cameraZoomer.StartZoomIn();
    }

    private IEnumerator PlayerMove()
    {
        while (true)
        {
            m_playerCharacter.Translate(m_moveSpeed * Time.deltaTime * m_move, Space.World);

            if (m_move != Vector3.zero)
            {
                m_playerCharacter.rotation = Quaternion.Slerp(m_playerCharacter.rotation, Quaternion.LookRotation(m_move), m_rotationSpeed * Time.deltaTime);
            }

            yield return null;
        }
    }
}
