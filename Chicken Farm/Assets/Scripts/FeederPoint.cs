using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeederPoint : MonoBehaviour
{
    [SerializeField] private bool m_isFree;

    private void Start()
    {
        MakeFeederFree();
    }

    public void MakeFeederFree()
    {
        m_isFree = true;
    }

    public void MakeFeederBusy()
    {
        m_isFree = false;
    }

    public bool GetFeederState()
    {
        return m_isFree;
    }
}
