//скорость патрулирования - 0.75
//скорость бега - 1.1

using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;
using System.Collections;

public class Patrol : MonoBehaviour
{
    [SerializeField] private Transform m_patrolKit;
    [SerializeField] private bool m_isFirstKit;
    public List<Transform> points = new List<Transform>();
    private int destPoint = 0;
    private NavMeshAgent agent;
    private EggBuyerInstance m_buyerInstance;
    private EggSellingStore m_eggSellingStore;

    private int m_pointsCount;


    private void Awake()
    {
        m_buyerInstance = GetComponent<EggBuyerInstance>();
    }

    void Start()
    {
        m_eggSellingStore = EggSellingStore.Instance;
        SetInPatrolKit();
        SetPatrolPoints();

        agent = GetComponent<NavMeshAgent>();
        agent.autoBraking = false;
        GotoNextPoint();
    }


    void GotoNextPoint()
    {
        // Returns if no points have been set up
        if (points.Count == 0)
            return;

        // Set the agent to go to the currently selected destination.
        agent.destination = points[destPoint].position;

        if (destPoint >= m_pointsCount)
        {
            if (!m_isFirstKit)
            {
                StartCoroutine(m_buyerInstance.DestroyWithEffects());
            }
            else
            {
                StartCoroutine(WaitAndChangePatrolKit());
            }
        }
        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        try
        {
            destPoint = (destPoint + 1) % points.Count;
        }
        catch { }
    }


    void Update()
    {
        // Choose the next destination point when the agent gets
        // close to the current one.
        if (!agent.pathPending && agent.remainingDistance < 0.5f)
            GotoNextPoint();
    }

    private void SetPatrolPoints()
    {
        for (int i = 0; i < m_patrolKit.transform.childCount; i++)
        {
            points.Add(m_patrolKit.transform.GetChild(i));
        }
        m_pointsCount = m_patrolKit.transform.childCount - 1;
    }

    private IEnumerator WaitAndChangePatrolKit()
    {
        m_buyerInstance.SetIdleAnimation();
        agent.isStopped = true;
        points.Clear();
        destPoint = 0;
        yield return new WaitForSeconds(1f);
        if (m_eggSellingStore.GetCurrentEggsAmount() >= 1)
        {
            m_buyerInstance.MakePurchase();
            m_buyerInstance.SetWalkingEggsBowlAnimation();
        }
        else
        {
            m_buyerInstance.MakeCustomerSad();
            m_buyerInstance.SetWalkingEmptyHandsAnimation();
        }
        agent.isStopped = false;
        SetOutPatrolKit();
        SetPatrolPoints();
        m_isFirstKit = false;
    }

    public void SetOutPatrolKit()
    {
        m_patrolKit = m_eggSellingStore.GetOutPatrolKit();
    }

    private void SetInPatrolKit()
    {
        m_patrolKit = m_eggSellingStore.GetInPatrolKit();
    }
}