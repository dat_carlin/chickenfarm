﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Chicken", menuName = "Chicken")]
public class ChickenShopItem : ScriptableObject
{
    public Sprite m_birdSprite;
    public int m_price;
    public ChickensLevel m_chickenLevel;
    public int m_eggsPerMinute;
    public int m_eggPrice;
    public string m_chickenName;


    private bool m_enableToSell;

    public void SetEnableToSell(bool state)
    {
        m_enableToSell = state;
    }

    public bool GetEnableToSell()
    {
        return m_enableToSell;
    }
}
