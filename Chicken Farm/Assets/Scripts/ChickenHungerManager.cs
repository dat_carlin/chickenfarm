using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ChickenNeedType
{
    None,
    Food = 10,
    Water = 20
}

public class ChickenHungerManager : MonoBehaviour
{
    [SerializeField] private GameObject m_waterIcon, m_foodIcon;

    private ChickenInstance m_chickenInstance;
    private ChickenMovement m_chickenMovement;

    private float m_currentEggTimer;
    [SerializeField] private float m_maxEggTimer;
    private bool m_enableDecreaseEggTimer;

    private float m_currentHungryValue;
    private float m_maxHungryValue = 25f;
    private bool m_enableDescreaseHungryValue;

    private float m_currentThirstyValue;
    private float m_maxThirstyValue = 40f;
    private bool m_enableDecreaseWaterValue;

    private Coroutine m_eggsTimerRoutine;
    private Coroutine m_hungryTimeRoutine;
    private Coroutine m_waterTimeRoutine;

    private float m_eggsTimeAfterReset;
    private float m_eggsDiffEggsTimer;

    private float m_hungryTimeAfterReset;
    private float m_hungryDiffEggsTimer;

    private float m_waterTimeAfterReset;
    private float m_waterDiffEggsTimer;

    private void Awake()
    {
        m_chickenInstance = GetComponent<ChickenInstance>();
        m_chickenMovement = GetComponent<ChickenMovement>();
    }

    private void Start()
    {
        StartAllTimers();
        HideHungryIcon();
        HideWaterIcon();
    }

    private void StartAllTimers()
    {
        m_eggsTimerRoutine = StartCoroutine(StartDecreaseEggsTimer(m_maxEggTimer));
        m_hungryTimeRoutine = StartCoroutine(StartHungryTimer(m_maxHungryValue));
        m_waterTimeRoutine = StartCoroutine(StartWaterTimer(m_maxThirstyValue));
    }

    #region Eggs Timer
    private IEnumerator StartDecreaseEggsTimer(float delay)
    {
        yield return new WaitForSeconds(delay);
        m_eggsTimeAfterReset = Time.time;
        m_chickenInstance.SpawnEgg();
        m_eggsTimerRoutine = StartCoroutine(StartDecreaseEggsTimer(m_maxEggTimer));
    }

    private void StopEggsTimer()
    {
        StopCoroutine(m_eggsTimerRoutine);
        float stoppedTimerValue = Time.time - m_eggsTimeAfterReset;
        m_eggsDiffEggsTimer = m_maxEggTimer - stoppedTimerValue;
    }
    #endregion

    #region Hungry Timer
    private IEnumerator StartHungryTimer(float delay)
    {
        yield return new WaitForSeconds(delay);
        m_hungryTimeAfterReset = Time.time;
        StopWater();
        ShowHungryIcon();
        CheckHungryThirsty();
        m_chickenInstance.GoToNearestFoodFeeder();

    }

    public IEnumerator StartFeedChicken()
    {
        m_chickenInstance.TurnOffMovement();
        yield return new WaitForSeconds(0.5f);
        FeedChicken();
        yield return new WaitForSeconds(0.5f);
        m_chickenInstance.TurnOnMovement();
        m_chickenMovement.EnableRandomMovement();
    }

    public IEnumerator StartWaterChicken()
    {
        m_chickenInstance.TurnOffMovement();
        yield return new WaitForSeconds(0.5f);
        WaterChicken();
        yield return new WaitForSeconds(0.5f);
        m_chickenInstance.TurnOnMovement();
        m_chickenMovement.EnableRandomMovement();
    }

    [ContextMenu("Feed Chicken")]
    private void FeedChicken()
    {
        HideHungryIcon();
        CheckEnableToStartEggsTimer();
        m_waterTimeRoutine = StartCoroutine(StartWaterTimer(m_waterDiffEggsTimer));
        StartCoroutine(StartHungryTimer(m_maxHungryValue));
        StartCoroutine(m_chickenInstance.PlayFeedAnimation());
    }

    private void StopHungry()
    {
        StopCoroutine(m_hungryTimeRoutine);
        float stoppedTimerValue = Time.time - m_hungryTimeAfterReset;
        m_hungryDiffEggsTimer = m_maxHungryValue - stoppedTimerValue;
    }
    #endregion

    #region Water Timer
    private IEnumerator StartWaterTimer(float delay)
    {
        yield return new WaitForSeconds(delay);

        m_waterTimeAfterReset = Time.time;
        StopHungry();
        ShowWaterIcon();
        CheckHungryThirsty();
        m_chickenInstance.GoToNearestWaterFeeder();
    }

    [ContextMenu("Water Chicken")]
    private void WaterChicken()
    {
        HideWaterIcon();
        CheckEnableToStartEggsTimer();
        m_hungryTimeRoutine = StartCoroutine(StartHungryTimer(m_hungryDiffEggsTimer));
        StartCoroutine(StartWaterTimer(m_maxHungryValue));
        StartCoroutine(m_chickenInstance.PlayFeedAnimation());
    }

    private void StopWater()
    {
        StopCoroutine(m_waterTimeRoutine);
        float stoppedTimerValue = Time.time - m_waterTimeAfterReset;
        m_waterDiffEggsTimer = m_maxThirstyValue - stoppedTimerValue;
    }
    #endregion

    #region Icons

    private void HideHungryIcon()
    {
        m_foodIcon.SetActive(false);
    }

    private void ShowHungryIcon()
    {
        m_foodIcon.SetActive(true);
    }

    private void HideWaterIcon()
    {
        m_waterIcon.SetActive(false);
    }

    private void ShowWaterIcon()
    {
        m_waterIcon.SetActive(true);
    }
    #endregion

    private void CheckEnableToStartEggsTimer()
    {
        m_eggsTimerRoutine = StartCoroutine(StartDecreaseEggsTimer(m_eggsDiffEggsTimer));
    }

    private void CheckHungryThirsty()
    {
        if (m_currentHungryValue <= 0 || m_currentThirstyValue <= 0)
        {
            StopEggsTimer();
        }
    }
}
