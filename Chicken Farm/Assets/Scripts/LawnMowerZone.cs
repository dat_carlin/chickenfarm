using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LawnMowerZone : MonoBehaviour
{
    private PlayerInstance m_playerInstance;

    private void Start()
    {
        m_playerInstance = PlayerInstance.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        m_playerInstance.StartLawnMowing();
    }

    private void OnTriggerExit(Collider other)
    {
        m_playerInstance.StopLawnMowing();
    }
}
