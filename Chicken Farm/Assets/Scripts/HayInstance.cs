using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HayInstance : MonoBehaviour
{
    private BoxCollider m_collider;
    private HayContainer m_hayContainer;

    private Rigidbody m_rigidbody;
    private ParabolaObjectMovement m_parabola;
    private HayConvertor m_hayConvector;

    private bool m_enableToGrab;

    private void Awake()
    {
        m_collider = GetComponent<BoxCollider>();
        m_rigidbody = GetComponent<Rigidbody>();
        m_parabola = GetComponent<ParabolaObjectMovement>();
    }

    private void Start()
    {
        m_hayContainer = HayContainer.Instance;
        StartCoroutine(EnableToGrab());
        JumpRandomDirection();
    }

    public IEnumerator EnableToGrab()
    {
        yield return new WaitForSeconds(5f);
        m_enableToGrab = true;
    }

    private void JumpRandomDirection()
    {
        transform.rotation = Random.rotation;
        m_rigidbody.AddForce(Vector3.up * 300f);
    }

    public IEnumerator JumpToHeap()
    {
        if (m_enableToGrab)
        {
            Transform newParent = m_hayContainer.GetHeapFreePoint();
            if (newParent != null)
            {
                m_hayContainer.AddHay(this);
                m_collider.enabled = false;
                m_rigidbody.useGravity = false;
                m_rigidbody.isKinematic = true;
                m_parabola.JumpToPointB(newParent, newParent.position);
                //transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
                yield return new WaitForSeconds(0.25f);
                m_parabola.enabled = false;
                transform.SetParent(newParent);
                transform.localPosition = Vector3.zero;
                transform.localScale = new Vector3(1f, 1f, 1f);
                transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
            }
        }
    }

    public void JumpByParabola(Transform pointB, bool defaultSettings)
    {
        if (defaultSettings)
        {
            m_parabola.JumpToPointB(pointB, pointB.position);
        }
        else
        {
            m_parabola.JumpToPointB(pointB, pointB.position, 4f, 1f);
        }
    }

    public void RemoveHay()
    {
        transform.SetParent(null);
        m_parabola.enabled = true;
        if (m_hayConvector == null)
        {
            m_hayConvector = HayConvertor.Instance;
        }
        JumpByParabola(m_hayConvector.m_endPoint, true);
        gameObject.AddComponent<ObjectDestroyer>();
    }
}
