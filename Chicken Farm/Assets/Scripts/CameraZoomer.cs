using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomer : MonoBehaviour
{
    public static CameraZoomer Instance;

    private bool m_isSimpleZoomedOut;
    private bool m_isLawnZoomedIn;

    private float m_zoomedOutValue = 40f;
    private float m_zoomedInValue = 35f;

    private float m_zoomSpeed = 1f;

    private Camera m_camera;

    private float m_currentZoomValue;

    private void Awake()
    {
        m_camera = Camera.main;
        Instance = this;
    }

    private void Start()
    {
        StartZoomIn();
    }

    private void Update()
    {
        m_camera.fieldOfView = Mathf.Lerp(m_camera.fieldOfView, m_currentZoomValue, Time.deltaTime * m_zoomSpeed);
    }

    //private void LateUpdate()
    //{
    //    if (Input.GetKeyDown(KeyCode.Space))
    //    {
    //        if (m_isZoomedOut)
    //        {
    //            StartZoomIn();
    //        }
    //        else
    //        {
    //            StartZoomOut();
    //        }
    //    }
    //}

    public void StartZoomOut()
    {
        if (!m_isLawnZoomedIn)
        {
            m_currentZoomValue = m_zoomedOutValue;
            m_isSimpleZoomedOut = true;
        }
    }

    public void StartZoomIn()
    {
        if (!m_isLawnZoomedIn)
        {
            m_currentZoomValue = m_zoomedInValue;
            m_isSimpleZoomedOut = false;
        }
    }

    public void ZoomOutLawnMowerZone()
    {
        m_isLawnZoomedIn = false;

        if (m_isSimpleZoomedOut)
        {
            StartZoomOut();
        }
    }

    public void ZoomInLawnMowerZone()
    {
        m_isLawnZoomedIn = true;

        m_currentZoomValue = m_zoomedInValue;
    }
}
