﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DollarSpawner : MonoBehaviour
{
    public static DollarSpawner Instance;
    [SerializeField] private GameObject m_dollarPrefab;

    private void Awake()
    {
        Instance = this;
    }

    public void SpawnDollars(Vector3 spawnPosition, int count)
    {
        for (int i = 0; i < count; i++)
        {
            Instantiate(m_dollarPrefab, spawnPosition, Quaternion.identity);
        }
    }
}
