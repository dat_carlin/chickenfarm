using System.Collections;
using UnityEngine;

public class GrainSupplier : MonoBehaviour
{
    public static GrainSupplier Instance;

    [SerializeField] private GameObject m_foodBucketPrefab;
    [SerializeField] private Transform m_bucketSpawnPoint;
    [SerializeField] private Animator m_animator;
    [SerializeField] private ParticleSystem m_grainParticles;
    [SerializeField] private CooldownCircle m_cooldownCircle;

    private float m_currentGrainAmount;

    private const string m_grainAmountAnimName = "GrainAmount";
    private float m_animationSpeed = 1f;

    private float m_addedAmount = 0.25f;
    private float m_removerAmount = 1f;
    private float m_maxGrainAmount = 4f;

    private bool m_canReduceTimer;
    private float m_spawnTimerMax = 1f;
    private float m_spawnTimerCurrent;


    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_spawnTimerCurrent = m_spawnTimerMax;
    }

    private void Update()
    {
        ChangeGrainAnimation();
        ReduceTimer();
    }

    private void ChangeGrainAnimation()
    {
        m_animator.SetFloat(m_grainAmountAnimName, m_currentGrainAmount, 1f, Time.deltaTime * m_animationSpeed);
    }

    public void AddAmount()
    {
        if (m_currentGrainAmount <= m_maxGrainAmount - m_addedAmount)
        {
            m_currentGrainAmount += m_addedAmount;
        }
    }

    public void PlayGrainParticles()
    {
        m_grainParticles.Play();
    }

    //public IEnumerator StartSpawningBuckets(int requestedAmount)
    //{
    //    if (requestedAmount > 0)
    //    //if (m_currentGrainAmount >= m_removerAmount && requestedAmount > 0)
    //    {
    //        SpawnFoodBucket();
    //        m_currentGrainAmount -= m_removerAmount;
    //        requestedAmount--;
    //        yield return new WaitForSeconds(0.15f);
    //        StartCoroutine(StartSpawningBuckets(requestedAmount));
    //    }
    //}

    private void SpawnFoodBucket()
    {
        GameObject foodBucket = Instantiate(m_foodBucketPrefab, m_bucketSpawnPoint.position, Quaternion.identity);

        FoodBucketInstance bucketInstance = foodBucket.GetComponent<FoodBucketInstance>();

        StartCoroutine(bucketInstance.JumpToHeap());
    }

    public void StartReceiving()
    {
        m_spawnTimerCurrent = m_spawnTimerMax;
        m_canReduceTimer = true;
        m_cooldownCircle.StartTimerVoid();
    }

    public void StopReceivingGrain()
    {
        m_canReduceTimer = false;
        m_spawnTimerCurrent = m_spawnTimerMax;
        m_cooldownCircle.StopCooling();
    }

    private void ReduceTimer()
    {
        if (m_canReduceTimer)
        {
            m_spawnTimerCurrent -= Time.deltaTime;
            if (m_spawnTimerCurrent <= 0)
            {
                m_cooldownCircle.StopCooling();
                m_spawnTimerCurrent = m_spawnTimerMax;
                SpawnFoodBucket();
                m_cooldownCircle.StartTimerVoid();
            }
        }
    }
}
