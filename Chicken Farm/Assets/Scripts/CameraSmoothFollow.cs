using UnityEngine;

public class CameraSmoothFollow : MonoBehaviour
{
    public static CameraSmoothFollow Instance;

    [SerializeField] private Transform m_target;
    [SerializeField] private float m_smoothTime = 0.4f;

    private Transform m_camTransform;
    private Vector3 m_offset;
    private Vector3 velocity = Vector3.zero;
    private float m_movementSpeed = 10f;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_camTransform = Camera.main.transform;
        m_offset = m_camTransform.position - m_target.position;
    }

    private void Update()
    {
        Vector3 desiredPosition = m_target.position + m_offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, m_smoothTime);
        transform.position = Vector3.Lerp(transform.position, smoothedPosition, m_movementSpeed * Time.deltaTime);
    }
}
