using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MovementState
{
    None,
    Standing = 10,
    Running = 20,
    Throwing = 30
}

public enum ObjectInHandsState
{
    Empty = 0,
    Chicken = 10,
    EggsBowl = 20,
    FoodBucket = 30,
    WaterBucket = 31,
    LawnMower = 40
}


public class PlayerMovement : MonoBehaviour
{
    public static PlayerMovement Instance;
    [SerializeField] private Joystick m_joystick;

    private Animator m_animator;

    private bool m_canChangeHandsState;

    private MovementState m_movementState;
    private ObjectInHandsState m_objectsInHandsState;

    private const float m_animationSpeed = 17.5f;
    private const string m_localMovementStateName = "LocalMovementState";
    private const string m_globalMovementStateName = "GlobalMovementState";
    private const string m_throwChickenAnimName = "Stickman_ThrowChicken";
    private const string m_globalMovementAnimName = "GlobalMovementState";

    private void Awake()
    {
        Instance = this;
        m_animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (m_canChangeHandsState)
        {
            switch (m_objectsInHandsState)
            {
                case ObjectInHandsState.Chicken:
                    {
                        m_animator.SetFloat(m_globalMovementStateName, 1f, 1f, Time.deltaTime * m_animationSpeed);
                        break;
                    }
                case ObjectInHandsState.LawnMower:
                    {
                        m_animator.SetFloat(m_globalMovementStateName, -0.5f, 1f, Time.deltaTime * m_animationSpeed);

                        break;
                    }
                case ObjectInHandsState.Empty:
                    {
                        m_animator.SetFloat(m_globalMovementStateName, 0f, 1f, Time.deltaTime * m_animationSpeed);
                        break;
                    }
                case ObjectInHandsState.EggsBowl:
                case ObjectInHandsState.FoodBucket:
                case ObjectInHandsState.WaterBucket:
                    {
                        m_animator.SetFloat(m_globalMovementStateName, -1f, 1f, Time.deltaTime * m_animationSpeed);
                        break;
                    }
            }
        }
        switch (m_movementState)
        {
            case MovementState.Standing:
                {
                    m_animator.SetFloat(m_localMovementStateName, 0f, 1f, Time.deltaTime * m_animationSpeed);
                    break;
                }
            case MovementState.Running:
                {
                    m_animator.SetFloat(m_localMovementStateName, 1f, 1f, Time.deltaTime * m_animationSpeed);
                    break;
                }
            case MovementState.Throwing:
                {
                    m_animator.SetFloat(m_localMovementStateName, -1f, 1f, Time.deltaTime * m_animationSpeed);
                    break;
                }
        }
    }

    public IEnumerator PlayThrowAnimation()
    {
        m_animator.Play(m_throwChickenAnimName);
        SetNewObjectInHands(ObjectInHandsState.Empty);
        yield return new WaitForSeconds(0.5f);
        m_animator.SetFloat(m_globalMovementStateName, 0f);
        m_animator.Play(m_globalMovementAnimName);
    }

    public void StartWalkingAnimation()
    {
        SetNewMovementState(MovementState.Running);
    }

    public void StopWalkingAnimation()
    {
        SetNewMovementState(MovementState.Standing);
    }


    private void SetNewMovementState(MovementState newState)
    {
        m_movementState = newState;
    }

    public void SetNewObjectInHands(ObjectInHandsState objectInHandsState)
    {
        m_objectsInHandsState = objectInHandsState;
    }


    public ObjectInHandsState GetObjectInHandState()
    {
        return m_objectsInHandsState;
    }

    public IEnumerator SetEggsBowlInHands()
    {
        SetNewObjectInHands(ObjectInHandsState.EggsBowl);
        m_canChangeHandsState = true;
        yield return new WaitForSeconds(2f);
        m_canChangeHandsState = false;
    }

    public IEnumerator SetLawnMoverlInHands()
    {
        SetNewObjectInHands(ObjectInHandsState.LawnMower);
        m_canChangeHandsState = true;
        yield return new WaitForSeconds(2f);
        m_canChangeHandsState = false;
    }


    public IEnumerator SetFoodBucketInHands()
    {
        SetNewObjectInHands(ObjectInHandsState.FoodBucket);
        m_canChangeHandsState = true;
        yield return new WaitForSeconds(2f);
        m_canChangeHandsState = false;
    }

    public IEnumerator SetWaterBucketInHands()
    {
        SetNewObjectInHands(ObjectInHandsState.WaterBucket);
        m_canChangeHandsState = true;
        yield return new WaitForSeconds(2f);
        m_canChangeHandsState = false;
    }

    public IEnumerator SetEmptyHands()
    {
        SetNewObjectInHands(ObjectInHandsState.Empty);
        m_canChangeHandsState = true;
        yield return new WaitForSeconds(2f);
        m_canChangeHandsState = false;
    }

    public IEnumerator SetChickenInHands()
    {
        SetNewObjectInHands(ObjectInHandsState.Chicken);
        m_canChangeHandsState = true;
        yield return new WaitForSeconds(2f);
        m_canChangeHandsState = false;
    }

    public void SetLawnMowerSettings()
    {
        StartCoroutine(SetLawnMoverlInHands());
        m_joystick.SetLawnMowerSpeed();
    }

    public void SetNormalSettings()
    {
        StartCoroutine(SetEmptyHands());
        m_joystick.SetNormalSpeed();
    }
}
