﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PseudoFOV : MonoBehaviour
{
    public static PseudoFOV Instance;

    [SerializeField] private List<Transform> m_chickensInView = new List<Transform>();


    private void Awake()
    {
        Instance = this;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 8) //chicken
        {
            AddChickenToList(other.transform);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            RemoveChickenFromList(other.transform);
        }
    }


    private void AddChickenToList(Transform newChicken)
    {
        m_chickensInView.Add(newChicken);
    }

    public void RemoveChickenFromList(Transform removeableChicken)
    {
        if (m_chickensInView.Contains(removeableChicken))
        {
            m_chickensInView.Remove(removeableChicken);
        }
    }

    public List<Transform> GetListOfChickens()
    {
        return m_chickensInView;
    }
}
