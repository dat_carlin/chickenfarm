using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassPillarInstance : MonoBehaviour
{
    private float m_delay = 35f;

    private BoxCollider m_boxCollider;


    private void Awake()
    {
        m_boxCollider = GetComponent<BoxCollider>();
    }

    public IEnumerator TurnOffForTime()
    {
        m_boxCollider.enabled = false;
        yield return new WaitForSeconds(m_delay);
        m_boxCollider.enabled = true;
    }
}
