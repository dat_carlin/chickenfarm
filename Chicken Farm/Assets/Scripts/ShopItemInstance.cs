﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItemInstance : MonoBehaviour
{
    [Header("Bird setup")]
    [SerializeField] private Image m_birdImage;
    [SerializeField] private Text m_birdPriceText;
    [SerializeField] private Text m_eggsPerMinuteText;
    [SerializeField] private Text m_eggPriceText;
    [SerializeField] private Text m_chickenNameText;
    [Space]
    [Header("Buy button")]
    [SerializeField] private Button m_buyButton;

    [SerializeField] private GameObject m_activeCoin;
    [SerializeField] private GameObject m_deactiveCoin;
    [SerializeField] private Color m_priceActiveColor;
    [SerializeField] private Color m_priceDeactiveColor;

    private ChickensLevel m_chickenLevel;

    private RectTransform m_rectTransform;
    private ShopManager m_shopManager;
    private ChickenSpawner m_chickenSpawner;
    private MoneyBankManager m_bankManager;

    private ChickenShopItem m_chickenShopItem;

    private int m_birdPrice;

    private void Awake()
    {
        m_rectTransform = GetComponent<RectTransform>();
        m_buyButton.onClick.AddListener(OnBuyButtonClick);
    }

    private void Start()
    {
        m_shopManager = ShopManager.Instance;
        m_chickenSpawner = ChickenSpawner.Instance;
        m_bankManager = MoneyBankManager.Instance;
        ResetTransform();
    }


    public void SetupShopItem(ChickenShopItem chickenShopItem)
    {
        m_chickenShopItem = chickenShopItem;

        m_chickenLevel = m_chickenShopItem.m_chickenLevel;
        m_birdPrice = m_chickenShopItem.m_price;
        m_birdImage.sprite = chickenShopItem.m_birdSprite;
        m_birdPriceText.text = m_birdPrice.ToString();

        m_eggsPerMinuteText.text = m_chickenShopItem.m_eggsPerMinute + " eggs/min";
        m_eggPriceText.text = "Egg cost " + m_chickenShopItem.m_eggPrice + " coins";
        m_chickenNameText.text = chickenShopItem.m_chickenName;
    }

    private void ResetTransform()
    {
        m_rectTransform.localPosition = Vector2.zero;
    }

    public void EnableBuyButton()
    {
        m_buyButton.interactable = true;
        m_deactiveCoin.SetActive(false);
        m_activeCoin.SetActive(true);
        m_birdPriceText.color = m_priceActiveColor;
    }

    public void DisableBuyButton()
    {
        m_buyButton.interactable = false;
        m_deactiveCoin.SetActive(true);
        m_activeCoin.SetActive(false);
        m_birdPriceText.color = m_priceDeactiveColor;
    }

    private void OnBuyButtonClick()
    {
        m_chickenSpawner.SpawnChicken(m_chickenLevel, m_shopManager.m_spawnPosition.position);
        PayToBank();
    }

    private void PayToBank()
    {
        m_bankManager.ReduceMoney(m_birdPrice);
    }

    public void CheckBuyingAbility()
    {
        if (m_bankManager.IsMoreThenBank(m_birdPrice))
        {
            DisableBuyButton();
        }
        else
        {
            EnableBuyButton();
        }
    }
}
