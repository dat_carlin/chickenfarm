using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatchUpManager : MonoBehaviour
{
    public static CatchUpManager Instance;

    [SerializeField] private GameObject m_catchUpButtonObject;

    private PlayerInstance m_playerInstance;

    private ChickenInstance m_currentChicken;

    private ThrowManager m_throwManager;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_throwManager = ThrowManager.Instance;
        m_playerInstance = PlayerInstance.Instance;
        TurnOffButton();
    }

    public void TurnOnButton(ChickenInstance newChicken)
    {
        m_currentChicken = newChicken;
        m_catchUpButtonObject.SetActive(true);
    }

    public void TurnOffButton()
    {
        m_catchUpButtonObject.SetActive(false);
    }

    public void OnCathUpButtonClick()
    {
        StartCoroutine(m_playerInstance.ReceiveChickenInHands(m_currentChicken));
        TurnOffButton();
        m_throwManager.TurnOnButton(m_currentChicken);
        m_currentChicken = null;

    }
}
