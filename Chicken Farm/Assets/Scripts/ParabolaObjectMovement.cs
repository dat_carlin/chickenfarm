using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParabolaObjectMovement : MonoBehaviour
{
    [HideInInspector] public Transform target;
    [Range(0.1f, 30f)]
    public float m_speed = 4f;

    private Vector3 _offset = new Vector3(0, 2, 0);
    private bool _reachedTarget = false;

    private float m_arcHeight = 2f;
    private float _time = 0;
    private Vector3 _startPoint;
    private Vector3 _targetPoint;
    private Vector3 _middlePoint;
    private Vector3 _point1;
    private Vector3 _point2;
    private Vector3 _endPosition;

    private bool m_canJump;

    public void JumpToPointB(Transform pointB, Vector3 endPosition)
    {
        _reachedTarget = false;
        //if (target == null) return;
        target = pointB;
        //m_objectInHands = objectInHands;
        _startPoint = transform.position;
        _endPosition = endPosition;
        _targetPoint = endPosition + (new Vector3(0, transform.localScale.y, 0) / 2);
        _middlePoint = (_startPoint * 2 + _targetPoint) / 3 + _offset;
        _point1 = _startPoint;
        _point2 = _middlePoint;
        _time = 0;
        m_canJump = true;
    }

    public void JumpToPointB(Transform pointB, Vector3 endPosition, float _speed, float _arkHeight)
    {
        m_speed = _speed;
        m_arcHeight = _arkHeight;
        _reachedTarget = false;
        //if (target == null) return;
        target = pointB;
        //m_objectInHands = objectInHands;
        _startPoint = transform.position;
        _endPosition = endPosition;
        _targetPoint = endPosition + (new Vector3(0, transform.localScale.y, 0) / 2);
        _middlePoint = (_startPoint * 2 + _targetPoint) / 3 + _offset;
        _point1 = _startPoint;
        _point2 = _middlePoint;
        _time = 0;
        m_canJump = true;
    }


    private void Update()
    {
        if (m_canJump)
        {
            if (target == null) return;
            if (_reachedTarget == true) return;

            _time += Time.deltaTime * m_speed;
            _point1 = Vector3.Lerp(_startPoint, _middlePoint, _time);

            _targetPoint = target.position + (new Vector3(0, transform.localScale.y, 0) / 2f);
            _middlePoint = (_startPoint * m_arcHeight + _targetPoint) / 3 + _offset;

            _point2 = Vector3.Lerp(_middlePoint, _targetPoint, _time);
            transform.position = Vector3.Lerp(_point1, _point2, _time);

            if (_point1 == _middlePoint && _point2 == _targetPoint)
            {
                _reachedTarget = true;
            }
        }
    }
}
