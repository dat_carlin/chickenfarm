using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggInstance : MonoBehaviour
{
    [SerializeField] private GameObject m_particle;

    [SerializeField] private int m_eggPrice = 5;

    private BoxCollider m_collider;
    private EggContainer m_eggContainer;

    private Rigidbody m_rigidbody;

    private ParabolaObjectMovement m_parabola;
    private EggReceiver m_eggReceiver;

    private void Awake()
    {
        m_parabola = GetComponent<ParabolaObjectMovement>();
        m_rigidbody = GetComponent<Rigidbody>();
        m_collider = GetComponent<BoxCollider>();
    }

    private void Start()
    {
        m_eggContainer = EggContainer.Instance;
        m_eggReceiver = EggReceiver.Instance;
    }

    public IEnumerator JumpToHeap()
    {
        Transform newParent = m_eggContainer.GetHeapFreePoint();
        if (newParent != null)
        {
            m_eggContainer.AddEgg(this);
            m_collider.enabled = false;
            m_rigidbody.useGravity = false;
            m_rigidbody.isKinematic = true;
            m_particle.SetActive(false);
            m_parabola.JumpToPointB(newParent, newParent.position);
            transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
            yield return new WaitForSeconds(0.25f);
            m_parabola.enabled = false;
            transform.SetParent(newParent);
            transform.localPosition = Vector3.zero;
            transform.localScale = new Vector3(10f, 10f, 10f);
            transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
        }
    }

    public void JumpByParabola(Transform pointB, bool defaultSettings)
    {
        if (defaultSettings)
        {
            m_parabola.JumpToPointB(pointB, pointB.position);
        }
        else
        {
            m_parabola.JumpToPointB(pointB, pointB.position, 4f, 1f);
        }
    }

    public void SellEgg()
    {
        transform.SetParent(null);
        m_parabola.enabled = true;
        JumpByParabola(m_eggReceiver.m_endPoint, true);
        //m_eggReceiver.SellEgg(m_eggPrice); //ТУТ МАГАЗИН ПЛАТИТ ДЕНЬГИ ЗА ПОСТАВКУ ЯИЦ
        gameObject.AddComponent<ObjectDestroyer>();
    }
}
