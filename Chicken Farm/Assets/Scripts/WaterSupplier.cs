using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSupplier : MonoBehaviour
{
    public static WaterSupplier Instance;

    [SerializeField] private GameObject m_waterBucketPrefab;
    [SerializeField] private Transform m_bucketSpawnPoint;
    [SerializeField] private CooldownCircle m_cooldownCircle;

    private bool m_canReduceTimer;
    private float m_spawnTimerMax = 1f;
    private float m_spawnTimerCurrent;

    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        ReduceTimer();
    }

    public IEnumerator StartSpawningBuckets(int requestedAmount)
    {
        if (requestedAmount > 0)
        {
            SpawnFoodBucket();
            requestedAmount--;
            yield return new WaitForSeconds(0.15f);
            StartCoroutine(StartSpawningBuckets(requestedAmount));
        }
    }

    private void SpawnFoodBucket()
    {
        GameObject waterBucket = Instantiate(m_waterBucketPrefab, m_bucketSpawnPoint.position, Quaternion.identity);

        WaterBucketInstance bucketInstance = waterBucket.GetComponent<WaterBucketInstance>();

        StartCoroutine(bucketInstance.JumpToHeap());
    }

    public void StartReceiving()
    {
        m_spawnTimerCurrent = m_spawnTimerMax;
        m_canReduceTimer = true;
        m_cooldownCircle.StartTimerVoid();
    }

    public void StopReceivingWater()
    {
        m_canReduceTimer = false;
        m_spawnTimerCurrent = m_spawnTimerMax;
        m_cooldownCircle.StopCooling();
    }

    private void ReduceTimer()
    {
        if (m_canReduceTimer)
        {
            m_spawnTimerCurrent -= Time.deltaTime;
            if (m_spawnTimerCurrent <= 0)
            {
                m_cooldownCircle.StopCooling();
                m_spawnTimerCurrent = m_spawnTimerMax;
                SpawnFoodBucket();
                m_cooldownCircle.StartTimerVoid();
            }
        }
    }
}
