using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInstance : MonoBehaviour
{
    public static PlayerInstance Instance;

    [SerializeField] private Transform m_chickenHolder;
    [SerializeField] private Transform m_aimPoint;
    [SerializeField] private Animator m_lawnMowerAnimator;
    [SerializeField] private GameObject m_lawnMowerMachine;

    private PseudoFOV m_pseudoFOV;
    private CatchUpManager m_catchUpManager;
    private PlayerMovement m_playerMovement;
    private CameraZoomer m_cameraZoomer;
    private ChickenInstance m_currentChickenInRadius;
    private FoodBucketContainer m_foodBucketContainer;
    private WaterBucketContainer m_waterBucketContainer;
    private GrabbingManager m_grabbingManager;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_playerMovement = GetComponent<PlayerMovement>();
        m_pseudoFOV = PseudoFOV.Instance;
        m_cameraZoomer = CameraZoomer.Instance;
        m_catchUpManager = CatchUpManager.Instance;
        m_foodBucketContainer = FoodBucketContainer.Instance;
        m_waterBucketContainer = WaterBucketContainer.Instance;
        m_grabbingManager = GrabbingManager.Instance;
    }


    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.layer)
        {
            case 9: //chicken sphere
                {
                    if (m_playerMovement.GetObjectInHandState() == ObjectInHandsState.Chicken || m_playerMovement.GetObjectInHandState() == ObjectInHandsState.Empty)
                    {
                        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 5f);
                        Collider tMin = null;
                        float minDist = Mathf.Infinity;
                        Vector3 currentPos = transform.position;
                        foreach (Collider t in hitColliders)
                        {
                            if (t.gameObject.layer == 9)
                            {
                                float dist = Vector3.Distance(t.transform.position, currentPos);
                                if (dist < minDist)
                                {
                                    tMin = t;
                                    minDist = dist;
                                }
                            }
                        }

                        ChickenInstance chicken = tMin.transform.parent.GetComponent<ChickenInstance>();
                        if (chicken == null) return;
                        if (!chicken.IsFlying())
                        {
                            m_currentChickenInRadius = chicken;
                            if (m_playerMovement.GetObjectInHandState() == ObjectInHandsState.Empty)
                            {
                                m_currentChickenInRadius.m_chickenMovement.StopMove();
                                m_grabbingManager.StartCooldown(chicken);
                                EggContainer.Instance.HardReset();
                            }
                        }
                    }


                    break;
                }
            case 10: //egg
                {
                    if (m_playerMovement.GetObjectInHandState() == ObjectInHandsState.EggsBowl || m_playerMovement.GetObjectInHandState() == ObjectInHandsState.Empty)
                    {
                        m_grabbingManager.FullResetCircle();
                        EggInstance egg = other.gameObject.GetComponent<EggInstance>();
                        StartCoroutine(egg.JumpToHeap());
                    }
                    break;
                }
            case 11: //egg receiver
                {
                    EggReceiver eggReceiver = other.gameObject.GetComponent<EggReceiver>();
                    if (m_playerMovement.GetObjectInHandState() == ObjectInHandsState.EggsBowl)
                    {
                        eggReceiver.StartSellingEggs();
                    }
                    break;
                }
            case 12: //food receiver
                {
                    FeederInstance feeder = other.gameObject.GetComponent<FeederInstance>();
                    switch (feeder.m_feederType)
                    {
                        case FeederType.Food:
                            {
                                if (m_playerMovement.GetObjectInHandState() == ObjectInHandsState.FoodBucket)
                                    feeder.StartFillFoodFeeder();
                                break;
                            }
                        case FeederType.Water:
                            {
                                if (m_playerMovement.GetObjectInHandState() == ObjectInHandsState.WaterBucket)
                                    feeder.StartFillWaterFeeder();
                                break;
                            }
                    }
                    break;
                }
            case 17: //hay
                {
                    HayInstance hay = other.gameObject.GetComponent<HayInstance>();
                    StartCoroutine(hay.JumpToHeap());
                    break;
                }
            case 18: //hay convertor
                {
                    HayConvertor hayConvertor = other.gameObject.GetComponent<HayConvertor>();
                    hayConvertor.StartConvertHays();
                    break;
                }
            case 19: //bucket
                {
                    FoodBucketInstance foodBucket = other.gameObject.GetComponent<FoodBucketInstance>();
                    if (foodBucket != null)
                    {
                        if (m_playerMovement.GetObjectInHandState() == ObjectInHandsState.Empty
                            || m_playerMovement.GetObjectInHandState() == ObjectInHandsState.FoodBucket)
                        {
                            StartCoroutine(foodBucket.JumpToHeap());
                        }
                        return;
                    }
                    WaterBucketInstance waterBucket = other.gameObject.GetComponent<WaterBucketInstance>();
                    if (waterBucket != null)
                    {
                        if (m_playerMovement.GetObjectInHandState() == ObjectInHandsState.Empty
                            || m_playerMovement.GetObjectInHandState() == ObjectInHandsState.WaterBucket)

                            StartCoroutine(waterBucket.JumpToHeap());
                    }

                    break;
                }
            case 20: //Grain Supplier
                {
                    if (m_playerMovement.GetObjectInHandState() == ObjectInHandsState.Empty || m_playerMovement.GetObjectInHandState() == ObjectInHandsState.FoodBucket)
                    {
                        GrainSupplier grainSupplier = other.gameObject.GetComponent<GrainSupplier>();
                        grainSupplier.StartReceiving();
                        //StartCoroutine(grainSupplier.StartSpawningBuckets(m_foodBucketContainer.GetFreePointsCount()));
                    }
                    break;
                }
            case 21: //WaterSupplier
                {
                    if (m_playerMovement.GetObjectInHandState() == ObjectInHandsState.Empty || m_playerMovement.GetObjectInHandState() == ObjectInHandsState.WaterBucket)
                    {
                        WaterSupplier waterSupplier = other.gameObject.GetComponent<WaterSupplier>();
                        waterSupplier.StartReceiving();
                        //StartCoroutine(waterSupplier.StartSpawningBuckets(m_waterBucketContainer.GetFreePointsCount()));
                    }
                    break;
                }
            case 29: //Dollar sphere
                {
                    DollarSphereCollider dollar = other.gameObject.GetComponent<DollarSphereCollider>();
                    dollar.PutMoney();
                    break;
                }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        switch (other.gameObject.layer)
        {
            case 9:
                {
                    m_grabbingManager.FullResetCircle();
                    if (m_currentChickenInRadius != null && !m_currentChickenInRadius.IsFlying())
                    {
                        m_currentChickenInRadius.m_chickenMovement.StartMove();
                    }
                    break;
                }
            case 20: //Grain Supplier
                {
                    GrainSupplier grainSupplier = other.gameObject.GetComponent<GrainSupplier>();
                    grainSupplier.StopReceivingGrain();
                    //StartCoroutine(grainSupplier.StartSpawningBuckets(m_foodBucketContainer.GetFreePointsCount()));
                    break;
                }
            case 21:
                {
                    WaterSupplier waterSupplier = other.gameObject.GetComponent<WaterSupplier>();
                    waterSupplier.StopReceivingWater();
                    break;
                }
        }
    }

    public void StartLawnMowing()
    {
        if (m_playerMovement.GetObjectInHandState() == ObjectInHandsState.Empty)
        {
            //m_lawnMowerMachine.SetActive(true);
            m_playerMovement.SetLawnMowerSettings();
            ShowLawnMover();
            m_cameraZoomer.ZoomInLawnMowerZone();
        }
    }

    public void StopLawnMowing()
    {
        if (m_playerMovement.GetObjectInHandState() == ObjectInHandsState.LawnMower)
        {
            //m_lawnMowerMachine.SetActive(false);
            m_playerMovement.SetNormalSettings();
            HideLawnMower();
            m_cameraZoomer.ZoomOutLawnMowerZone();
        }
    }

    private void ShowLawnMover()
    {
        m_lawnMowerAnimator.Play("ShowLawnMower");
    }

    private void HideLawnMower()
    {
        m_lawnMowerAnimator.Play("HideLawnMower");
    }

    public IEnumerator ReceiveChickenInHands(ChickenInstance chicken)
    {
        StartCoroutine(m_playerMovement.SetChickenInHands());
        yield return new WaitForSeconds(0.075f);
        chicken.JumpToPlayerHands(m_chickenHolder, m_chickenHolder.position);
    }

    public IEnumerator ThrowChickenFromHands(ChickenInstance chicken)
    {
        StartCoroutine(m_playerMovement.PlayThrowAnimation());
        yield return new WaitForSeconds(0.05f);

        List<Transform> chickensInView = m_pseudoFOV.GetListOfChickens();

        if (chickensInView.Count > 0)
        {
            int atempts = chickensInView.Count;
            for (int i = 0; i < chickensInView.Count; i++)
            {
                ChickenInstance chickenInstance = chickensInView[i].GetComponent<ChickenInstance>();

                if (chickenInstance.GetChickenLevel() == chicken.GetChickenLevel())
                {

                    chicken.StartFlightToChicken(chickensInView[i]);
                    break;
                }
                atempts--;

                if (atempts <= 0)
                {
                    chicken.StartFlightForward(transform);
                }
            }
            //chicken.StartFlightToChicken(chickensInView[0]);
        }
        else
        {
            chicken.StartFlightForward(transform);
        }
    }
}
