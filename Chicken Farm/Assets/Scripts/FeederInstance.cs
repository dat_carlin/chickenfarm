using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FeederType
{
    Water,
    Food
}

public class FeederInstance : MonoBehaviour
{
    [SerializeField] private FeederPoint[] m_feederPoints;
    public FeederType m_feederType;

    [SerializeField] private Animator m_animator;
    [SerializeField] private Transform m_endPoint;

    private FoodBucketContainer m_foodBucketContainer;
    private WaterBucketContainer m_waterBucketContainer;

    private List<ChickenInstance> m_foodQueue = new List<ChickenInstance>();
    private List<ChickenInstance> m_waterQueue = new List<ChickenInstance>();

    [SerializeField] private ParticleSystem m_receivingParticles;
    [Space]
    [SerializeField] private float m_currentGrainAmount;

    private const string m_grainAmountAnimName = "GrainHeight";
    private float m_animationSpeed = 1f;

    private float m_addedAmount = 0.25f;
    private float m_removerAmount = 0.25f;
    private float m_maxGrainAmount = 1f;

    private void Start()
    {
        m_currentGrainAmount = m_maxGrainAmount;
        m_foodBucketContainer = FoodBucketContainer.Instance;
        m_waterBucketContainer = WaterBucketContainer.Instance;
        StartCoroutine(CheckQueue());
    }

    public FeederPoint GetFreeFeederPoint()
    {
        FeederPoint feederPoint = null;

        for (int i = 0; i < m_feederPoints.Length; i++)
        {
            if (m_feederPoints[i].GetFeederState() == true) //true = is free
            {
                feederPoint = m_feederPoints[i];
                feederPoint.MakeFeederBusy();
                StartCoroutine(ResetPoint(feederPoint));
                //ReduceGrainSize();
                break;
            }
        }

        return feederPoint;
    }

    private void Update()
    {
        ChangeGrainAnimation();
    }

    private void ChangeGrainAnimation()
    {
        m_animator.SetFloat(m_grainAmountAnimName, m_currentGrainAmount, 1f, Time.deltaTime * m_animationSpeed);
    }

    public void ReduceGrainSize(ChickenInstance chicken)
    {
        if (m_currentGrainAmount >= m_removerAmount)
        {
            m_currentGrainAmount -= m_removerAmount;
            if (m_currentGrainAmount < 0.1f)
            {
                m_currentGrainAmount = 0f;
            }
        }
        else
        {
            switch (m_feederType)
            {
                case FeederType.Water:
                    {
                        AddChickenToQueueWater(chicken);
                        break;
                    }
                case FeederType.Food:
                    {
                        AddChickenToQueueFood(chicken);
                        break;
                    }
            }
        }

    }

    public void AddGrainSize()
    {
        if (m_currentGrainAmount <= m_maxGrainAmount - m_addedAmount)
        {
            m_currentGrainAmount += m_addedAmount;
            StartCoroutine(PlayReceivingParticles());
            //m_currentGrainAmount = 1;
        }
    }

    private IEnumerator ResetPoint(FeederPoint feederPoint)
    {
        yield return new WaitForSeconds(10f);
        feederPoint.MakeFeederFree();
    }

    public FeederType GetFeederType()
    {
        return m_feederType;
    }

    public void AddChickenToQueueFood(ChickenInstance chicken)
    {
        m_foodQueue.Add(chicken);
    }

    public void AddChickenToQueueWater(ChickenInstance chicken)
    {
        m_waterQueue.Add(chicken);
    }

    private IEnumerator CheckQueue()
    {
        if (m_foodQueue.Count > 0)
        {
            m_foodQueue[0].GoToNearestFoodFeeder();
            m_foodQueue.RemoveAt(0);
        }
        if (m_waterQueue.Count > 0)
        {
            m_waterQueue[0].GoToNearestWaterFeeder();
            m_waterQueue.RemoveAt(0);
        }
        yield return new WaitForSeconds(2f);
        StartCoroutine(CheckQueue());
    }

    public void StartFillFoodFeeder()
    {

        int neededAmount = (int)((m_maxGrainAmount - m_currentGrainAmount) / m_addedAmount);
        print(neededAmount);
        if (m_currentGrainAmount <= 0)
        {
            StartCoroutine(m_foodBucketContainer.RemoveBuckets(m_endPoint, neededAmount, this));
        }
        else
        {
            StartCoroutine(m_foodBucketContainer.RemoveBuckets(m_endPoint, neededAmount, this));
        }
    }

    public void StartFillWaterFeeder()
    {
        int neededAmount = (int)((m_maxGrainAmount - m_currentGrainAmount) / m_addedAmount);

        if (m_currentGrainAmount <= 0)
        {
            StartCoroutine(m_waterBucketContainer.RemoveBuckets(m_endPoint, neededAmount, this));
        }
        else
        {
            StartCoroutine(m_waterBucketContainer.RemoveBuckets(m_endPoint, neededAmount, this));
        }
    }

    public bool IsFeederFilled()
    {
        return m_currentGrainAmount > m_removerAmount;
    }

    private IEnumerator PlayReceivingParticles()
    {
        yield return new WaitForSeconds(0.1f);
        m_receivingParticles.Play();
    }
}
