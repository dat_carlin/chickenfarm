using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
    [SerializeField] private Button m_debugRestartButton;

    private void Awake()
    {
        m_debugRestartButton.onClick.AddListener(RestartLevel);
    }

    

    private void RestartLevel()
    {
        SceneManager.LoadScene(0);
    }
}
