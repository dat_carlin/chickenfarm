using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CooldownCircle : MonoBehaviour
{
    [SerializeField] private Image m_cooldown;
    [SerializeField] private GameObject m_background;
    private bool m_coolingDown;
    private float m_waitTime = 1f;

    private ChickenInstance m_currentChicken;
    private PlayerInstance m_playerInstance;
    private GrabbingManager m_grabbingManager;
    private Coroutine CircleRoutine;

    private void Start()
    {
        m_playerInstance = PlayerInstance.Instance;
        Reset();
        m_background.SetActive(false);
        m_grabbingManager = GrabbingManager.Instance;
    }

    void Update()
    {
        if (m_coolingDown == true)
        {
            m_cooldown.fillAmount += 1.0f / m_waitTime * Time.deltaTime;
        }
    }

    public float GetCooldownTimerValue()
    {
        return m_waitTime;
    }

    private void Reset()
    {
        m_cooldown.fillAmount = 0f;
    }

    [ContextMenu("Start timer")]
    public void StartTimerVoid(ChickenInstance chicken)
    {
        m_currentChicken = chicken;
        CircleRoutine = StartCoroutine(StartTimer());
    }

    public void StartTimerVoid()
    {
        CircleRoutine = StartCoroutine(StartTimer());
    }

    public void StopTimer()
    {
        if (CircleRoutine == null) return;
        StopCoroutine(CircleRoutine);
    }

    public IEnumerator StartTimer()
    {
        m_background.SetActive(true);
        m_coolingDown = true;
        yield return new WaitForSeconds(m_waitTime);
        StopCooling();
        StartCoroutine(m_playerInstance.ReceiveChickenInHands(m_currentChicken));
        m_grabbingManager.SetChickenInHands();
    }

    public void StopCooling()
    {
        m_background.SetActive(false);
        m_coolingDown = false;
        Reset();
        StopTimer();
    }
}
