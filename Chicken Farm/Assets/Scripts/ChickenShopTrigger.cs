﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenShopTrigger : MonoBehaviour
{
    [SerializeField] ShopManager m_shopManager;

    private void OnTriggerEnter(Collider other)
    {
        m_shopManager.ActivateShop();
    }
}
